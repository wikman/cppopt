/**
 * @file   test_basic.cpp
 * @author John Wikman
 * @brief  Tests basic use cases of an argparser.
 */

#include <iostream>

#include <gtest/gtest.h>

#include <cppopt/param.hpp>
#include <cppopt/parser.hpp>
#include <cppopt/positional.hpp>

#ifndef UNITTESTS_CHECKMEMLEAK
// This will cause mid-process exit, so no memleak check here
TEST(basic, NothingAddedToParser) {
    // Note: cppopt::parser::parse is not a const function. Need to create
    // copies for independent assertions.
    cppopt::parser parser("No description.");
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3movetest = parser;
    cppopt::parser pcopy3 = std::move(pcopy3movetest);
    //cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;

    pcopy2 = std::move(pcopy3); // Test a move for good measures

    // Default behavior is exit on error
    // (Both help prints and exit errors should display a usage)
    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EXIT(pcopy2.parse({"progname", "-h"}), testing::ExitedWithCode(0), "usage:");
    EXPECT_EXIT(pcopy3.parse({"progname", "--help"}), testing::ExitedWithCode(0), "usage:");
    EXPECT_DEATH(pcopy4.parse({"progname", "my_string"}), "usage:"); // invalid positional
    EXPECT_DEATH(pcopy5.parse({"progname", "--my-flag"}), "usage:"); // invalid optional
    EXPECT_DEATH(pcopy6.parse({}), "usage:"); // missing progname
}
#endif /* UNITTESTS_CHECKMEMLEAK */


#ifndef UNITTESTS_CHECKMEMLEAK
// This will cause mid-process exit, so no memleak check here
TEST(basic, OnlyOnePositional) {
    std::string myarg = "init";
    cppopt::parser parser("Only a single positional added.");
    parser.add(cppopt::positional<std::string>("MY_ARG", "my arg.", &myarg));
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;

    EXPECT_EQ(myarg, "init");
    EXPECT_NO_THROW(pcopy1.parse({"progname", "cmdline-input"}));
    EXPECT_EQ(myarg, "cmdline-input");

    EXPECT_DEATH(pcopy2.parse({"progname", "arg1", "arg2"}), "usage:"); // too many positionals
}
#endif /* UNITTESTS_CHECKMEMLEAK */


TEST(basic, OnlyOneOption) {
    std::string myparam = "init";
    cppopt::parser parser("Only a single option added (2 with the help option).");
    parser.add(cppopt::param<std::string>('m', "my-param", {"VALUE"}, "description...", &myparam));
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;

    EXPECT_EQ(myparam, "init");
    EXPECT_NO_THROW(pcopy1.parse({"progname", "--my-param", "used_long_name"}));
    EXPECT_EQ(myparam, "used_long_name");

    EXPECT_EQ(myparam, "used_long_name");
    EXPECT_NO_THROW(pcopy2.parse({"progname", "-m", "short_name_used"}));
    EXPECT_EQ(myparam, "short_name_used");

    EXPECT_EQ(myparam, "short_name_used");
    EXPECT_NO_THROW(pcopy3.parse({"progname", "--my-param", "1st time",
                                              "--my-param", "2nd time"}));
    EXPECT_EQ(myparam, "2nd time");
}
