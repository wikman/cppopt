/**
 * @file   test_compound.cpp
 * @author John Wikman
 * @brief  Tests compound parsing properties for short options.
 */

#include <cstdint>
#include <iostream>
#include <tuple>

#include <gtest/gtest.h>

#include <cppopt/flag.hpp>
#include <cppopt/incrementer.hpp>
#include <cppopt/multi_param.hpp>
#include <cppopt/param.hpp>
#include <cppopt/parser.hpp>

TEST(compound, option_single_param) {
    // Tests compound parsing with a single parameterized option
    bool testflag = false;
    std::string s = "init";
    std::size_t count = 0;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::flag('t', "test", "Test flag.", &testflag),
        cppopt::param<std::string>('s', "string", {"SVAL"}, "Test param.", &s),
        cppopt::incrementer<std::size_t>('c', "count", "Test incrementer.", &count)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(testflag, false);
    EXPECT_EQ(s, "init");
    EXPECT_EQ(count, 0);

    EXPECT_NO_THROW(pcopy2.parse({"progname", "-tcs", "value1"}));
    EXPECT_EQ(testflag, true);
    EXPECT_EQ(s, "value1");
    EXPECT_EQ(count, 1);

    testflag = false;
    s = "init";
    count = 0;
    EXPECT_NO_THROW(pcopy3.parse({"progname", "-ccc", "-ccs=value2"}));
    EXPECT_EQ(testflag, false);
    EXPECT_EQ(s, "value2");
    EXPECT_EQ(count, 5);

    testflag = false;
    s = "init";
    count = 0;
    EXPECT_NO_THROW(pcopy4.parse({"progname", "-scct"}));
    EXPECT_EQ(testflag, false);
    EXPECT_EQ(s, "cct");
    EXPECT_EQ(count, 0);

    testflag = false;
    s = "init";
    count = 0;
    EXPECT_NO_THROW(pcopy5.parse({"progname", "-sVALUE3", "-sVALUE4-tc"}));
    EXPECT_EQ(testflag, false);
    EXPECT_EQ(s, "VALUE4-tc");
    EXPECT_EQ(count, 0);
}

TEST(compound, option_multi_param) {
    // Tests compound parsing with a multiple parameterized options
    bool testflag = false;
    std::string s = "init";
    std::size_t count = 0;
    std::tuple<int, int> tup(0, 0);
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::flag('t', "test", "Test flag.", &testflag),
        cppopt::param<std::string>('s', "string", {"SVAL"}, "Test param.", &s),
        cppopt::incrementer<std::size_t>('c', "count", "Test incrementer.", &count),
        cppopt::param<int, int>('T', "tup", {"FST", "SND"}, "Test param with multiple values.", &tup)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(testflag, false);
    EXPECT_EQ(s, "init");
    EXPECT_EQ(count, 0);
    EXPECT_EQ(tup, std::make_tuple(0, 0));

    EXPECT_NO_THROW(pcopy2.parse({"progname", "-tsT=0", "-T=0", "1"}));
    EXPECT_EQ(testflag, true);
    EXPECT_EQ(s, "T=0");
    EXPECT_EQ(count, 0);
    EXPECT_EQ(tup, std::make_tuple(0, 1));

    testflag = false;
    s = "init";
    count = 0;
    tup = std::make_tuple(0, 0);
    EXPECT_NO_THROW(pcopy3.parse({"progname", "-tcccT100", "20", "-cs99"}));
    EXPECT_EQ(testflag, true);
    EXPECT_EQ(s, "99");
    EXPECT_EQ(count, 4);
    EXPECT_EQ(tup, std::make_tuple(100, 20));

    // Throw since "T=0" is argument to "-s", and "1" is an unexpected
    // positional
    EXPECT_THROW(pcopy4.parse({"progname", "-sT=0", "1"}), std::invalid_argument);
}
