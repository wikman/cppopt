/**
 * @file   test_options.cpp
 * @author John Wikman
 * @brief  Tests various option properies.
 *
 * Focuses more on isolated tests of various option types.
 */

#include <cstdint>
#include <iostream>
#include <tuple>
#include <vector>

#include <gtest/gtest.h>

#include <cppopt/flag.hpp>
#include <cppopt/incrementer.hpp>
#include <cppopt/multi_param.hpp>
#include <cppopt/param.hpp>
#include <cppopt/parser.hpp>

TEST(option, flag) {
    // Tests the flag option
    bool testflag = false;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::flag('t', "test", "Test flag.", &testflag)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(testflag, false);

    EXPECT_NO_THROW(pcopy2.parse({"progname", "--test"}));
    EXPECT_EQ(testflag, true);

    testflag = false;
    EXPECT_NO_THROW(pcopy3.parse({"progname", "-t"}));
    EXPECT_EQ(testflag, true);

    testflag = true;
    EXPECT_NO_THROW(pcopy4.parse({"progname", "-t"}));
    EXPECT_EQ(testflag, false);

    testflag = true;
    EXPECT_NO_THROW(pcopy5.parse({"progname", "-t", "-t"}));
    EXPECT_EQ(testflag, false);
}


TEST(option, incrementer) {
    // Tests the incrementer option
    std::uint32_t count = 0;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::incrementer<std::uint32_t>('t', "test", "Test incrementer.", &count)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(count, 0);

    EXPECT_NO_THROW(pcopy2.parse({"progname", "--test"}));
    EXPECT_EQ(count, 1);

    count = 0;
    EXPECT_NO_THROW(pcopy3.parse({"progname", "-t"}));
    EXPECT_EQ(count, 1);

    count = 0;
    EXPECT_NO_THROW(pcopy4.parse({"progname", "-t", "--test", "-t"}));
    EXPECT_EQ(count, 3);
}


TEST(option, param__single_value) {
    // Tests the param option on a single value
    std::string s = "init";
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::param<std::string>('t', "test", {"S_VALUE"}, "Test param.", &s)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;
    cppopt::parser pcopy7 = parser;
    cppopt::parser pcopy8 = parser;
    cppopt::parser pcopy9 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(s, "init");

    EXPECT_NO_THROW(pcopy2.parse({"progname", "-t", "value1"}));
    EXPECT_EQ(s, "value1");

    s = "init";
    EXPECT_NO_THROW(pcopy3.parse({"progname", "--test", "value2"}));
    EXPECT_EQ(s, "value2");

    s = "init";
    EXPECT_NO_THROW(pcopy4.parse({"progname", "-t=value3"}));
    EXPECT_EQ(s, "value3");

    s = "init";
    EXPECT_NO_THROW(pcopy5.parse({"progname", "--test=value4"}));
    EXPECT_EQ(s, "value4");

    s = "init";
    EXPECT_NO_THROW(pcopy6.parse({"progname", "--test=value5", "-t", "value6"}));
    EXPECT_EQ(s, "value6");

    s = "init";
    EXPECT_NO_THROW(pcopy7.parse({"progname", "-t=value7", "-t", "value8", "--test", "value9"}));
    EXPECT_EQ(s, "value9");

    // Missing values
    s = "init";
    EXPECT_THROW(pcopy8.parse({"progname", "--test"}), std::invalid_argument);
    EXPECT_THROW(pcopy9.parse({"progname", "-t"}), std::invalid_argument);
}


TEST(option, param__multiple_values) {
    // Tests the param option on multiple values
    std::string s = "init";
    std::uint32_t i = 0x0;
    int i2 = 0;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::param<std::string, std::uint32_t, int>(
            't', "test", {"S_VALUE", "I_VALUE", "I2_VALUE"},
            "Test param with multiple values.",
            &s, &i, &i2
        )
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;
    cppopt::parser pcopy7 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(s, "init");
    EXPECT_EQ(i, 0x0);
    EXPECT_EQ(i2, 0);

    EXPECT_NO_THROW(pcopy2.parse({"progname", "--test", "value1", "0x13", "-5"}));
    EXPECT_EQ(s, "value1");
    EXPECT_EQ(i, 0x13);
    EXPECT_EQ(i2, -5);

    s = "init";
    i = 0x0;
    i2 = 0;
    EXPECT_NO_THROW(pcopy3.parse({"progname", "-t=value2", "0b00100100", "300"}));
    EXPECT_EQ(s, "value2");
    EXPECT_EQ(i, 0x24);
    EXPECT_EQ(i2, 300);

    s = "init";
    i = 0x0;
    i2 = 0;
    EXPECT_NO_THROW(pcopy4.parse({"progname", "-t", "value3", "0x100", "44"}));
    EXPECT_EQ(s, "value3");
    EXPECT_EQ(i, 0x100);
    EXPECT_EQ(i2, 44);

    // Missing values
    s = "init";
    i = 0x0;
    i2 = 0;
    EXPECT_THROW(pcopy5.parse({"progname", "--test"}), std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "--test", "value4"}), std::invalid_argument);
    EXPECT_THROW(pcopy7.parse({"progname", "--test", "value5", "0x33"}), std::invalid_argument);
}


TEST(option, param__tuple_value) {
    // Tests the param option on a tuple target value
    std::tuple<std::string, std::uint32_t, int> tup("init", 0x0, 0);
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::param<std::string, std::uint32_t, int>(
            't', "test", {"S_VALUE", "I_VALUE", "I2_VALUE"},
            "Test param with a tuple target value.",
            &tup
        )
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;
    cppopt::parser pcopy7 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(tup, std::make_tuple("init", 0x0, 0));

    EXPECT_NO_THROW(pcopy2.parse({"progname", "--test", "value1", "0x13", "-5"}));
    EXPECT_EQ(tup, std::make_tuple("value1", 0x13, -5));

    tup = std::make_tuple("init", 0x0, 0);
    EXPECT_NO_THROW(pcopy3.parse({"progname", "-t=value2", "0b00100100", "300"}));
    EXPECT_EQ(tup, std::make_tuple("value2", 0x24, 300));

    tup = std::make_tuple("init", 0x0, 0);
    EXPECT_NO_THROW(pcopy4.parse({"progname", "-t", "value3", "0x100", "44"}));
    EXPECT_EQ(tup, std::make_tuple("value3", 0x100, 44));

    // Missing values
    tup = std::make_tuple("init", 0x0, 0);
    EXPECT_THROW(pcopy5.parse({"progname", "--test"}), std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "--test", "value4"}), std::invalid_argument);
    EXPECT_THROW(pcopy7.parse({"progname", "--test", "value5", "0x33"}), std::invalid_argument);
}


TEST(option, multi_param__single_vector) {
    // Tests the multi_param option
    std::vector<std::string> sv;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::multi_param<std::string>(
            't', "test", {"SV_VALUE"},
            "Test multi_param with a single target vector.",
            &sv
        )
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(sv, std::vector<std::string>({}));

    EXPECT_NO_THROW(pcopy2.parse({"progname", "--test", "A"}));
    EXPECT_EQ(sv, std::vector<std::string>({"A"}));

    sv = std::vector<std::string>();
    EXPECT_NO_THROW(pcopy3.parse({"progname", "-t", "X1",
                                              "--test", "X2",
                                              "-t=X3",
                                              "--test=X4"}));
    EXPECT_EQ(sv, std::vector<std::string>({"X1", "X2", "X3", "X4"}));

    // Missing values
    sv = std::vector<std::string>();
    EXPECT_THROW(pcopy4.parse({"progname", "--test"}), std::invalid_argument);
    EXPECT_THROW(pcopy5.parse({"progname", "--test", "value4", "-t"}), std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "-t"}), std::invalid_argument);
}


TEST(option, multi_param__multiple_vector) {
    // Tests the multi_param option
    std::vector<std::string> sv;
    std::vector<std::uint32_t> iv;
    std::vector<int> iv2;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::multi_param<std::string, std::uint32_t, int>(
            't', "test", {"SV_VALUE", "IV_VALUE", "IV2_VALUE"},
            "Test multi_param with multiple target vectors.",
            &sv, &iv, &iv2
        )
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(sv, std::vector<std::string>({}));
    EXPECT_EQ(iv, std::vector<std::uint32_t>({}));
    EXPECT_EQ(iv2, std::vector<int>({}));

    EXPECT_NO_THROW(pcopy2.parse({"progname", "--test", "value1", "0x13", "-5"}));
    EXPECT_EQ(sv, std::vector<std::string>({"value1"}));
    EXPECT_EQ(iv, std::vector<std::uint32_t>({0x13}));
    EXPECT_EQ(iv2, std::vector<int>({-5}));

    sv = std::vector<std::string>();
    iv = std::vector<std::uint32_t>();
    iv2 = std::vector<int>();
    EXPECT_NO_THROW(pcopy3.parse({"progname", "--test=X1", "0x100", "55",
                                              "-t=X2", "0b101", "-300",
                                              "-t", "X3", "99", "4000",
                                              "--test", "X4", "0x0", "1"}));
    EXPECT_EQ(sv, std::vector<std::string>({"X1", "X2", "X3", "X4"}));
    EXPECT_EQ(iv, std::vector<std::uint32_t>({0x100, 0x5, 99, 0x0}));
    EXPECT_EQ(iv2, std::vector<int>({55, -300, 4000, 1}));

    // Missing values
    sv = std::vector<std::string>();
    iv = std::vector<std::uint32_t>();
    iv2 = std::vector<int>();
    EXPECT_THROW(pcopy4.parse({"progname", "--test"}), std::invalid_argument);
    EXPECT_THROW(pcopy5.parse({"progname", "--test", "value2"}), std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "-t", "value3", "0x66"}), std::invalid_argument);
}


TEST(option, multi_param__tuple_vector) {
    // Tests the multi_param option
    std::vector<std::tuple<std::string, std::uint32_t, int>> tv;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::multi_param<std::string, std::uint32_t, int>(
            't', "test", {"S_VALUE", "I_VALUE", "I2_VALUE"},
            "Test multi_param with a tuple vector.",
            &tv
        )
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    auto cmp1 = std::vector<std::tuple<std::string, std::uint32_t, int>>({});
    EXPECT_EQ(tv, cmp1);

    EXPECT_NO_THROW(pcopy2.parse({"progname", "--test", "value1", "0x13", "-5"}));
    auto cmp2 = std::vector<std::tuple<std::string, std::uint32_t, int>>({
        std::make_tuple("value1", 0x13, -5)
    });
    EXPECT_EQ(tv, cmp2);

    tv = std::vector<std::tuple<std::string, std::uint32_t, int>>();
    EXPECT_NO_THROW(pcopy3.parse({"progname", "--test=X1", "0x100", "55",
                                              "-t=X2", "0b101", "-300",
                                              "-t", "X3", "99", "4000",
                                              "--test", "X4", "0x0", "1"}));
    auto cmp3 = std::vector<std::tuple<std::string, std::uint32_t, int>>({
        std::make_tuple("X1", 0x100, 55),
        std::make_tuple("X2", 0x5,   -300),
        std::make_tuple("X3", 99,    4000),
        std::make_tuple("X4", 0x0,   1)
    });
    EXPECT_EQ(tv, cmp3);

    // Missing values
    tv = std::vector<std::tuple<std::string, std::uint32_t, int>>();
    EXPECT_THROW(pcopy4.parse({"progname", "--test"}), std::invalid_argument);
    EXPECT_THROW(pcopy5.parse({"progname", "--test", "value2"}), std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "-t", "value3", "0x66"}), std::invalid_argument);
}

