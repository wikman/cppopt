/**
 * @file   test_combinations.cpp
 * @author John Wikman
 * @brief  Tests various combinations of options and positionals
 */

#include <cstdint>
#include <iostream>
#include <tuple>

#include <gtest/gtest.h>

#include <cppopt/flag.hpp>
#include <cppopt/incrementer.hpp>
#include <cppopt/multi_param.hpp>
#include <cppopt/multi_positional.hpp>
#include <cppopt/param.hpp>
#include <cppopt/parser.hpp>
#include <cppopt/positional.hpp>


TEST(combo, one_option_and_positional) {
    // Tests combination parsing with one option and one positional
    int i = 0;
    std::string s = "init";
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::positional<int>("IVAL", "Test positional.", &i, cppopt::OPTIONAL),
        cppopt::param<std::string>('s', "string", {"SVAL"}, "Test param.", &s)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname"}));
    EXPECT_EQ(i, 0);
    EXPECT_EQ(s, "init");

    EXPECT_NO_THROW(pcopy2.parse({"progname", "-s", "value1", "33"}));
    EXPECT_EQ(i, 33);
    EXPECT_EQ(s, "value1");

    i = 0;
    s = "init";
    EXPECT_NO_THROW(pcopy3.parse({"progname", "900", "-s", "value2"}));
    EXPECT_EQ(i, 900);
    EXPECT_EQ(s, "value2");

    i = 0;
    s = "init";
    EXPECT_NO_THROW(pcopy4.parse({"progname", "-s", "value3"}));
    EXPECT_EQ(i, 0);
    EXPECT_EQ(s, "value3");

    i = 0;
    s = "init";
    EXPECT_NO_THROW(pcopy5.parse({"progname", "96"}));
    EXPECT_EQ(i, 96);
    EXPECT_EQ(s, "init");
}


TEST(combo, optional_and_required) {
    // Tests combination parsing with optional and required options and
    // positionals
    std::string s1 = "init";
    std::string s2 = "init";
    int i1 = 0;
    int i2 = 0;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::positional<std::string>("S1VAL", "Test required positional.", &s1),
        cppopt::positional<std::string>("S2VAL", "Test optional positional.", &s2, cppopt::OPTIONAL),
        cppopt::param<int>("i1", {"I1VAL"}, "Test required param.", &i1),
        cppopt::param<int>("i2", {"I1VAL"}, "Test required param.", &i2, cppopt::REQUIRED)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;
    cppopt::parser pcopy7 = parser;

    EXPECT_NO_THROW(pcopy1.parse({"progname", "foo", "bar", "--i1=13", "--i2=17"}));
    EXPECT_EQ(s1, "foo");
    EXPECT_EQ(s2, "bar");
    EXPECT_EQ(i1, 13);
    EXPECT_EQ(i2, 17);

    s1 = "init";
    s2 = "init";
    i1 = 0;
    i2 = 0;
    EXPECT_NO_THROW(pcopy2.parse({"progname", "babar", "--i1=0x300", "--i2=9"}));
    EXPECT_EQ(s1, "babar");
    EXPECT_EQ(s2, "init");
    EXPECT_EQ(i1, 0x300);
    EXPECT_EQ(i2, 9);

    s1 = "init";
    s2 = "init";
    i1 = 0;
    i2 = 0;
    EXPECT_NO_THROW(pcopy3.parse({"progname", "cheese", "--i2=31"}));
    EXPECT_EQ(s1, "cheese");
    EXPECT_EQ(s2, "init");
    EXPECT_EQ(i1, 0);
    EXPECT_EQ(i2, 31);

    // Missing required arguments
    EXPECT_THROW(pcopy4.parse({"progname"}), std::invalid_argument);
    EXPECT_THROW(pcopy5.parse({"progname", "foo"}), std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "--i2=5"}), std::invalid_argument);
    EXPECT_THROW(pcopy7.parse({"progname", "foo", "--i1=99"}), std::invalid_argument);
}

