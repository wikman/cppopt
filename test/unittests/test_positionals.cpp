/**
 * @file   test_positionals.cpp
 * @author John Wikman
 * @brief  Tests various option properies.
 *
 * Focuses on isolated tests of the positional arguments.
 */

#include <cstdint>
#include <iostream>
#include <tuple>
#include <vector>

#include <gtest/gtest.h>

#include <cppopt/multi_positional.hpp>
#include <cppopt/parser.hpp>
#include <cppopt/positional.hpp>


TEST(positional, positional) {
    // Tests the standard positional with a single argument
    std::string s = "init";
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::positional<std::string>("S_VALUE", "Test positional", &s)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;

    // Should be required by default, throw exception if missing
    EXPECT_THROW(pcopy1.parse({"progname"}), std::invalid_argument);

    s = "init";
    EXPECT_NO_THROW(pcopy2.parse({"progname", "value1"}));
    EXPECT_EQ(s, "value1");
}


TEST(positional, multi_positional__single_vector) {
    // Tests multi positional with a single target vector
    std::vector<std::string> v;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::multi_positional<std::string>({"FILES"}, "Test multi_positional", &v)
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;

    // Should be required by default, throw exception if missing
    EXPECT_THROW(pcopy1.parse({"progname"}), std::invalid_argument);

    v = std::vector<std::string>();
    EXPECT_NO_THROW(pcopy2.parse({"progname", "foo"}));
    EXPECT_EQ(v, std::vector<std::string>({"foo"}));

    v = std::vector<std::string>();
    EXPECT_NO_THROW(pcopy3.parse({"progname", "foo", "bar", "babar"}));
    EXPECT_EQ(v, std::vector<std::string>({"foo", "bar", "babar"}));
}


TEST(positional, multi_positional__multiple_vector) {
    // Tests multi positional with multiple target vectors
    std::vector<std::string> v1;
    std::vector<int> v2;
    std::vector<std::string> v3;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::multi_positional<std::string, int, std::string>(
            {"FILE", "SIZE", "DIR"},
            "Test multi_positional", &v1, &v2, &v3
        )
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;

    // Should be required by default, throw exception if missing
    EXPECT_THROW(pcopy1.parse({"progname"}), std::invalid_argument);

    v1 = std::vector<std::string>();
    v2 = std::vector<int>();
    v3 = std::vector<std::string>();
    EXPECT_NO_THROW(pcopy2.parse({"progname", "foo", "50", "/etc"}));
    EXPECT_EQ(v1, std::vector<std::string>({"foo"}));
    EXPECT_EQ(v2, std::vector<int>({50}));
    EXPECT_EQ(v3, std::vector<std::string>({"/etc"}));

    v1 = std::vector<std::string>();
    v2 = std::vector<int>();
    v3 = std::vector<std::string>();
    EXPECT_NO_THROW(pcopy3.parse({"progname", "foo",   "300", "/usr",
                                              "bar",   "512", "/opt",
                                              "babar", "63",  "/etc"}));
    EXPECT_EQ(v1, std::vector<std::string>({"foo", "bar", "babar"}));
    EXPECT_EQ(v2, std::vector<int>({300, 512, 63}));
    EXPECT_EQ(v3, std::vector<std::string>({"/usr", "/opt", "/etc"}));

    v1 = std::vector<std::string>();
    v2 = std::vector<int>();
    v3 = std::vector<std::string>();
    // Missing positional arguments
    EXPECT_THROW(pcopy4.parse({"progname", "foo"}),
                 std::invalid_argument);
    EXPECT_THROW(pcopy5.parse({"progname", "foo", "300"}),
                 std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "foo", "300", "/usr",
                                           "bar", "512"}),
                 std::invalid_argument);
}


TEST(positional, multi_positional__tuple_vector) {
    // Tests multi positional with multiple target vectors
    std::vector<std::tuple<std::string, int, std::string>> tv;
    cppopt::parser parser;
    parser.exit_on_error(false);
    parser.add(
        cppopt::multi_positional<std::string, int, std::string>(
            {"FILE", "SIZE", "DIR"},
            "Test multi_positional", &tv
        )
    );
    cppopt::parser pcopy1 = parser;
    cppopt::parser pcopy2 = parser;
    cppopt::parser pcopy3 = parser;
    cppopt::parser pcopy4 = parser;
    cppopt::parser pcopy5 = parser;
    cppopt::parser pcopy6 = parser;

    // Should be required by default, throw exception if missing
    EXPECT_THROW(pcopy1.parse({"progname"}), std::invalid_argument);

    tv = std::vector<std::tuple<std::string, int, std::string>>();
    EXPECT_NO_THROW(pcopy2.parse({"progname", "foo", "50", "/etc"}));
    auto cmp2 = std::vector<std::tuple<std::string, int, std::string>>({
        std::make_tuple("foo", 50, "/etc")
    });
    EXPECT_EQ(tv, cmp2);

    tv = std::vector<std::tuple<std::string, int, std::string>>();
    EXPECT_NO_THROW(pcopy3.parse({"progname", "foo",   "300", "/usr",
                                              "bar",   "512", "/opt",
                                              "babar", "63",  "/etc"}));
    auto cmp3 = std::vector<std::tuple<std::string, int, std::string>>({
        std::make_tuple("foo", 300, "/usr"),
        std::make_tuple("bar", 512, "/opt"),
        std::make_tuple("babar", 63, "/etc")
    });
    EXPECT_EQ(tv, cmp3);

    tv = std::vector<std::tuple<std::string, int, std::string>>();
    // Missing positional arguments
    EXPECT_THROW(pcopy4.parse({"progname", "foo"}),
                 std::invalid_argument);
    EXPECT_THROW(pcopy5.parse({"progname", "foo", "300"}),
                 std::invalid_argument);
    EXPECT_THROW(pcopy6.parse({"progname", "foo", "300", "/usr",
                                           "bar", "512"}),
                 std::invalid_argument);
}
