#!/usr/bin/env python3

import argparse
import os
import sys


def print_error(msg, file=None, line=None):
    """Convenience function to print error to stderr."""
    output = f"\033[1;31m[ERROR]\033[0m In file {file}"
    if line is not None:
        output += f" on line {line}" % line
    output += f":\n{' '*8}{msg}\n"
    sys.stderr.write(output)


def main():
    """Attempts to find a license notice in the top doxygen comment in one or
    more C++ file. Exits with code 0 if an exact match was found in every file.
    Otherwise it will exit with code 1 and print an error message."""
    parser = argparse.ArgumentParser()
    parser.add_argument("license", metavar="LICENSE", type=os.path.realpath,
                        help="The reference file containing the license notice "
                             "that will be used to verify the notice in the "
                             "C++ file.")
    parser.add_argument("cppfiles", metavar="C++FILES", type=str,
                        nargs="+", help="C++ files to be checked.")
    args = parser.parse_args()

    license_lines = []
    with open(args.license, "r") as f:
        license_lines = [l.strip() for l in f.read().strip().split("\n")]
        if len("".join(license_lines)) == 0:
            print_error("Empty license file.", file=args.license)
            sys.exit(1)

    retcode = 0
    for cppfile in args.cppfiles:
        code = check_file(license_lines,
                          os.path.realpath(cppfile),
                          cppfile_display_name=cppfile)
        if code != 0:
            retcode = code

    sys.exit(retcode)


def check_file(license_lines, cppfile, cppfile_display_name=None):
    """Checks a specific C++ file. Returns 0 if was OK. Otherwise 1 is
    returned."""

    if cppfile_display_name is None:
        cppfile_display_name = cppfile

    doxygen_contents = []
    with open(cppfile, "r") as f:
        lines = f.readlines()
        if len(lines) == 0:
            print_error("Empty C++ file", file=cppfile_display_name)
            return 1
        if lines[0][0:3] != "/**":
            print_error("First line is not start of doxygen comment.",
                        file=cppfile_display_name, line=0)
            return 1

        doxygen_contents.append(lines[0][3:].strip())

        endidx = None
        for i in range(1, len(lines)):
            if lines[i][0:3] == " */":
                endidx = i
                break

            if lines[i][0:2] != " *":
                print_error("Doxygen content line does not start with \" *\".",
                            file=cppfile_display_name, line=i)
                return 1

            doxygen_contents.append(lines[i][2:].strip())

        if endidx is None:
            print_error("No doxygen ending found. A doxygen ending has to "
                        "start a line with \" */\".",
                        file=cppfile_display_name)
            return 1

    found_match = False
    for i in range(len(doxygen_contents)):
        # Try find a match license notice in doxygen comments
        if doxygen_contents[i] != license_lines[0]:
            continue

        if i + len(license_lines) > len(doxygen_contents):
            # The remaining lines cannot fit the license notice
            break

        doxypart = doxygen_contents[i:i+len(license_lines)]

        all_lines_match = True
        for doxyline, licenseline in zip(doxypart, license_lines):
            if doxyline != licenseline:
                all_lines_match = False

        if all_lines_match:
            found_match = True
            break

    if not found_match:
        print_error("Could not find a matching license notice in the top "
                    "doxygen comment.",
                    file=cppfile_display_name)
        return 1

    sys.stderr.write(f"\033[1;32m[OK]\033[0m {cppfile_display_name}: has valid license notice\n")
    return 0


if __name__ == "__main__":
    main()
