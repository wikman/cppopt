#!/usr/bin/env python3

import argparse
import os
import sys


def print_error(msg, file=None, line=None):
    """Convenience function to print error to stderr."""
    output = f"\033[1;31m[ERROR]\033[0m In file {file}"
    if line is not None:
        output += f" on line {line}" % line
    output += f":\n{' '*8}{msg}\n"
    sys.stderr.write(output)


def main():
    """Checks various code formatting properties of C++ files."""
    parser = argparse.ArgumentParser()
    parser.add_argument("cppfiles", metavar="C++FILES", type=str,
                        nargs="+", help="C++ files to be checked.")
    parser.add_argument("--no-crlf", dest="no_crlf",
                        action="store_const", const=True, default=False,
                        help="Checks for CRLF line endings.")
    parser.add_argument("--no-tab", dest="no_tab",
                        action="store_const", const=True, default=False,
                        help="Checks for tab characters.")
    parser.add_argument("--no-trailing-whitespace", dest="no_trailing_whitespace",
                        action="store_const", const=True, default=False,
                        help="Checks for trailing whitespaces.")
    args = parser.parse_args()

    retcode = 0
    for cppfile in args.cppfiles:
        code = check_file(os.path.realpath(cppfile),
                          args,
                          cppfile_display_name=cppfile)
        if code != 0:
            retcode = code

    sys.exit(retcode)


def check_file(cppfile, args, cppfile_display_name=None):
    """Checks a specific C++ file. Returns 0 if was OK. Otherwise 1 is
    returned."""

    if cppfile_display_name is None:
        cppfile_display_name = cppfile

    # Report encounters in terms of line numbers
    crlf_encounters = []
    tab_encounters = []
    trailing_whitespace_encounters = []
    with open(cppfile, "rb") as f:
        lines = f.read().split(b"\n")
        for l_idx, line in zip(range(1, len(lines) + 1), lines):
            if args.no_tab:
                # Check that no tab characters are on the line
                if b"\t" in line:
                    tab_encounters.append(l_idx)
            if args.no_crlf:
                # Check that the line does not contain a carriage return
                if b"\r" in line:
                    crlf_encounters.append(l_idx)
            if args.no_trailing_whitespace and len(line) > 0:
                # Check that no trailing linespaces are encountered
                if line[-1:] == b" ":
                    trailing_whitespace_encounters.append(l_idx)

    error_messages = []
    if len(crlf_encounters) > 0:
        lineidxs = ", ".join([str(i) for i in crlf_encounters])
        error_messages.append(f"CRLF found on lines {lineidxs}")
    if len(tab_encounters) > 0:
        lineidxs = ", ".join([str(i) for i in tab_encounters])
        error_messages.append(f"Tab characters found on lines {lineidxs}")
    if len(trailing_whitespace_encounters) > 0:
        lineidxs = ", ".join([str(i) for i in trailing_whitespace_encounters])
        error_messages.append(f"Trailing whitespaces found on lines {lineidxs}")

    if len(error_messages) > 0:
        msgs = "\n - ".join([""] + error_messages)
        sys.stderr.write(f"\033[1;31m[ERROR]\033[0m Errors in {cppfile_display_name}:" +
                         f"{msgs}\n")
        return 1

    sys.stderr.write(f"\033[1;32m[OK]\033[0m {cppfile_display_name}: No formatting errors found\n")
    return 0


if __name__ == "__main__":
    main()
