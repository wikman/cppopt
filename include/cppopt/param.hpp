/**
 * @file   param.hpp
 * @author John Wikman
 * @brief  Implements a class for parameterized options.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_PARAM_HPP
#define CPPOPT_PARAM_HPP

#include <array>
#include <cstddef>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "cast_optval.hpp"
#include "exception.hpp"
#include "modifier.hpp"
#include "option_base.hpp"

namespace cppopt {

/**
 * @brief An option that acceps parameters.
 *
 * Can specify one or more parameters to be accepted by this option. A param
 * option can be toggled multiple times, but it will only record the latest
 * value(s) it was toggled on.
 *
 * Example usage:
 * @code{.cpp}
 * int v = 0;
 * std::string s1 = "s1";
 * std::string s2 = "s2";
 * std::tuple<int, int> tv(0, 0);
 *
 * auto parser = cppopt::parser(...);
 * parser.add(
 *     cppopt::param<int>('v', "value", {"VALUE"}, "Sets the value v", &v),
 *     cppopt::param<std::string, std::string>(
 *         "s1s2", {"S1_VALUE", "S2_VALUE"},
 *         "Sets the values of s1 and s2", &s1, &s2
 *     ),
 *     cppopt::param<int, int>(
 *         'T', "tuple-val", {"VAL1", "VAL2"},
 *        "Sets the tuple value tv", &tv
 *     )
 * );
 *
 * ...
 *
 * // For the `v` param
 * parser.parse({"prog"});                     // v == 0
 * parser.parse({"prog", "-v2"});              // v == 2
 * parser.parse({"prog", "--value=1"});        // v == 1
 * parser.parse({"prog", "--value=1", "-v5"}); // v == 5
 *
 * // For the `s1` and `s2` param
 * parser.parse({"prog"});                     // s1 == "s1", s2 == "s2"
 * parser.parse({"prog", "--s1s2", "X", "Y"}); // s1 == "X",  s2 == "Y"
 * // s1 == "foo",  s2 == "bar"
 * parser.parse({"prog", "--s1s2", "C", "B",
 *                       "--s1s2", "foo", "bar"});
 *
 * // For the `tv` param
 * parser.parse({"prog"});                                  // tv == (0, 0)
 * parser.parse({"prog", "-T", "5", "9"});                  // tv == (5, 9)
 * parser.parse({"prog", "-T", "1", "7", "-T", "40", "2"}); // tv == (40, 2)
 * @endcode
 */
template <typename... ParamTypes>
class param: public option_base {
    static_assert(sizeof...(ParamTypes) > 0, "A param option must have at least one type argument.");
private:
    /**
     * @brief Internal enum used to determine how the values are referenced.
     */
    enum class refmode {TUPLE, INDIVIDUAL_REFS};
public:
    /**
     * @brief Constructor for param option including a short name.
     *        (Individual pointer version)
     * @param shortopt The short name of this option.
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param value_refs Pointers to the individual values that shall be set
     *                   when this option is toggled.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    param(char shortopt, const std::string& longopt,
          const std::array<std::string, sizeof...(ParamTypes)>& metavars,
          const std::string& description,
          ParamTypes* ...value_refs,
          modifier mod = NONE):
        option_base(shortopt,
                    longopt,
                    std::vector<std::string>(metavars.begin(), metavars.end()),
                    description,
                    mod),
        m_refmode(param::refmode::INDIVIDUAL_REFS),
        m_tuple_value_ref(nullptr),
        m_invididual_value_refs(std::tuple<ParamTypes*...>(value_refs...))
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for param option with only a long name.
     *        (Individual pointer version)
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param value_refs Pointers to the individual values that shall be set
     *                   when this option is toggled.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    param(const std::string& longopt,
          const std::array<std::string, sizeof...(ParamTypes)>& metavars,
          const std::string& description,
          ParamTypes* ...value_refs,
          modifier mod = NONE):
        param('\0', longopt, metavars, description, value_refs..., mod)
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for param option including a short name.
     *        (Tuple version)
     * @param shortopt The short name of this option.
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param tuple_ref Pointer to the tuple to be assigned with the values
     *                  that the param was toggled on.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    param(char shortopt, const std::string& longopt,
          const std::array<std::string, sizeof...(ParamTypes)>& metavars,
          const std::string& description,
          std::tuple<ParamTypes...>* tuple_ref,
          modifier mod = NONE):
        option_base(shortopt,
                    longopt,
                    std::vector<std::string>(metavars.begin(), metavars.end()),
                    description,
                    mod),
        m_refmode(param::refmode::TUPLE),
        m_tuple_value_ref(tuple_ref)
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for param option with only a long name.
     *        (Tuple version)
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param tuple_ref Pointer to the tuple to be assigned with the values
     *                  that the param was toggled on.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    param(const std::string& longopt,
          const std::array<std::string, sizeof...(ParamTypes)>& metavars,
          const std::string& description,
          std::tuple<ParamTypes...>* tuple_ref,
          modifier mod = NONE):
        param('\0', longopt, metavars, description, tuple_ref, mod)
    {
        // Do nothing here
    }

    /**
     * @brief Destructor.
     */
    virtual ~param() {}

    /**
     * @brief Copy-constructor.
     * @param other The param object to copy from.
     */
    param(const param& other):
        option_base(other),
        m_refmode(other.m_refmode),
        m_tuple_value_ref(other.m_tuple_value_ref),
        m_invididual_value_refs(other.m_invididual_value_refs)
    {
        // No special behavior
    }

    /**
     * @brief Move-constructor.
     * @param other The param object to move into this object.
     */
    param(param&& other) noexcept: option_base(other)
    {
        std::swap(m_refmode, other.m_refmode);
        std::swap(m_tuple_value_ref, other.m_tuple_value_ref);
        std::swap(m_invididual_value_refs, other.m_invididual_value_refs);
    }

    /**
     * @brief Copy-assignment.
     * @param other The param object to copy from.
     * @return A reference to this object after the copy operation.
     */
    param& operator=(const param& other)
    {
        option_base::operator=(other);
        m_refmode = other.m_refmode;
        m_tuple_value_ref = other.m_tuple_value_ref;
        m_invididual_value_refs = other.m_invididual_value_refs;
        return *this;
    }

    /**
     * @brief Move-assignment.
     * @param other The param object to move into this object.
     * @return A reference to this object after the move operation.
     */
    param& operator=(param&& other) noexcept
    {
        option_base::operator=(other);
        std::swap(m_refmode, other.m_refmode);
        std::swap(m_tuple_value_ref, other.m_tuple_value_ref);
        std::swap(m_invididual_value_refs, other.m_invididual_value_refs);
        return *this;
    }

    /**
     * @brief Returns a dynamically allocated copy of the param object.
     * @return An std::shared_ptr containing a dynamically allocated pointer to a
     *         cppopt::param object.
     */
    std::shared_ptr<option_base> copy() const override
    {
        return std::shared_ptr<option_base>(new param<ParamTypes...>(*this));
    }

private:
    /**
     * @brief Base case for individual value reference partial toggle.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @note Only used if specified param with individual target values.
     */
    template <typename TLast>
    void m_individual_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - 1;

        // Apply last parameter
        m_individual_cast_and_assign<TLast, idx>(params[idx]);
    }

    /**
     * @brief Partial toggle on a single argument when using individual value
     *        pointers. Recursively invoked.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @note Only used if specified param with individual target values.
     *
     * Apply one parameter, and then recurse until base case is reached.
     */
    template <typename TFirst, typename TNext, typename... TRest>
    void m_individual_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - (sizeof...(TRest)) - 2;

        // Apply first parameter
        m_individual_cast_and_assign<TFirst, idx>(params[idx]);

        // Recurse with remaining parameters
        m_individual_internal_partial_toggle<TNext, TRest...>(params);
    }

    /**
     * @brief Cast and assign the parameterized value into its corresponding
     *        value.
     * @param param The value to cast and assign.
     * @note Only used if specified param with individual target values.
     *
     * @todo Introduce a new exception here that also specifies the parameter
     *       index.
     */
    template <typename ArgType, std::size_t idx>
    void m_individual_cast_and_assign(const std::string& param)
    {
        ArgType* ref = std::get<idx>(m_invididual_value_refs);

        *ref = cast_optval<ArgType>(param);
    }

    /**
     * @brief Base case for tuple pointer partial toggle.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @return Single-value tuple with the result of the last parameter.
     * @note Only used if specified param with a tuple pointer.
     */
    template <typename TLast>
    std::tuple<TLast> m_tuple_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - 1;

        // Convert last parameter
        return std::make_tuple(cast_optval<TLast>(params[idx]));
    }

    /**
     * @brief Partial toggle on a single argument when using a tuple pointer.
     *        Recursively invoked.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @return Tuple with the partial results.
     * @note Only used if specified param with a tuple pointer.
     */
    template <typename TFirst, typename TNext, typename... TRest>
    std::tuple<TFirst, TNext, TRest...> m_tuple_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - (sizeof...(TRest)) - 2;

        // Convert first parameter
        std::tuple<TFirst> t_head = std::make_tuple(cast_optval<TFirst>(params[idx]));

        // Fetch the other parameters.
        std::tuple<TNext, TRest...> t_tail = m_tuple_internal_partial_toggle<TNext, TRest...>(params);

        return std::tuple_cat(t_head, t_tail);
    }

protected:
    /**
     * @brief Internal toggle specific to the param class.
     * @param params Parameters for this option.
     *
     * @note Internal cppopt usage only.
     */
    virtual void m_internal_toggle(const std::vector<std::string>& params)
    {
        switch (m_refmode) {
        case param::refmode::TUPLE:
            *m_tuple_value_ref = m_tuple_internal_partial_toggle<ParamTypes...>(params);
            break;
        case param::refmode::INDIVIDUAL_REFS:
            m_individual_internal_partial_toggle<ParamTypes...>(params);
            break;
        }
    }

private:
    /**
     * @brief The reference mode being used.
     */
    param::refmode m_refmode;

    /**
     * @brief Pointer to tuple to be assigned on toggle.
     * @note Only used if m_refmode is set to TUPLE.
     */
    std::tuple<ParamTypes...>* m_tuple_value_ref;

    /**
     * @brief Individual value pointers to be assigned on toggle.
     * @note Only used if m_refmode is set to INDIVIDUAL_REFS.
     */
    std::tuple<ParamTypes*...> m_invididual_value_refs;
};

} /* namespace cppopt */

#endif /* CPPOPT_PARAM_HPP */
