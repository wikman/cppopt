/**
 * @file   incrementer.hpp
 * @author John Wikman
 * @brief  Option that increments on each toggle.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_INCREMENTER_HPP
#define CPPOPT_INCREMENTER_HPP

#include <array>
#include <cstddef>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "cast_optval.hpp"
#include "exception.hpp"
#include "modifier.hpp"
#include "option_base.hpp"

namespace cppopt {

/**
 * @brief An option that increments on each toggle.
 *
 * The provided counter pointer is incremented by prefix ++ as `++counter`.
 * As such, any type is a valid template argument as long as it implements
 * prefix incrementation.
 *
 * Example usage:
 * @code{.cpp}
 * int counter = 0;
 *
 * auto parser = cppopt::parser(...);
 * parser.add(
 *     cppopt::incrementer('v', "verbosity", "Specify verbosity level", &counter)
 * );
 *
 * ...
 *
 * parser.parse({"prog"});              // counter == 0
 * parser.parse({"prog", "-v"});        // counter == 1
 * parser.parse({"prog", "-vv", "-v"}); // counter == 3
 *
 * // counter == 5
 * parser.parse({"prog", "--verbosity", "-vvv", "--verbosity"});
 * @endcode
 */
template <typename CounterType>
class incrementer: public option_base {
public:
    /**
     * @brief Constructor for incrementer option including a short name.
     * @param shortopt The short name of this option.
     * @param longopt The long name of this option.
     * @param description Description shown when printing the help message.
     * @param counter_ref Pointer to the counter to be incremented on
     *                    toggle.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    incrementer(char shortopt, const std::string& longopt,
                const std::string& description,
                CounterType* counter_ref,
                modifier mod = NONE):
        option_base(shortopt,
                    longopt,
                    {},
                    description,
                    mod),
        m_counter_ref(counter_ref)
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for incrementer option with only a long name.
     * @param longopt The long name of this option.
     * @param description Option description shown when printing the help
     *                    message.
     * @param counter_ref Pointer to the counter to be incremented on
     *                    toggle.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    incrementer(const std::string& longopt,
                const std::string& description,
                CounterType* counter_ref,
                modifier mod = NONE):
        incrementer('\0', longopt, description, counter_ref, mod)
    {
        // Do nothing here
    }

    /**
     * @brief Destructor.
     */
    virtual ~incrementer() {}

    /**
     * @brief Copy-constructor.
     * @param other The incrementer object to copy from.
     */
    incrementer(const incrementer& other):
        option_base(other),
        m_counter_ref(other.m_counter_ref)
    {
        // No special behavior
    }

    /**
     * @brief Move-constructor.
     * @param other The incrementer object to move into this object.
     */
    incrementer(incrementer&& other) noexcept:
        option_base(other)
    {
        std::swap(m_counter_ref, other.m_counter_ref);
    }

    /**
     * @brief Copy-assignment.
     * @param other The incrementer object to copy from.
     * @return A reference to this incrementer object after the copy operation.
     */
    incrementer& operator=(const incrementer& other)
    {
        option_base::operator=(other);
        m_counter_ref = other.m_counter_ref;
        return *this;
    }

    /**
     * @brief Move-assignment.
     * @param other The incrementer object to move into this object.
     * @return A reference to this incrementer object after the move.
     */
    incrementer& operator=(incrementer&& other) noexcept
    {
        option_base::operator=(other);
        std::swap(m_counter_ref, other.m_counter_ref);
        return *this;
    }

    /**
     * @brief Returns a dynamically allocated copy of the incrementer object.
     * @return An std::shared_ptr containing a dynamically allocated pointer to
     *         a cppopt::incrementer object.
     */
    std::shared_ptr<option_base> copy() const override
    {
        return std::shared_ptr<option_base>(new incrementer<CounterType>(*this));
    }

protected:
    /**
     * @brief Internal toggle specific to the incrementer class.
     * @param params Parameters for this option.
     *
     * @note Internal cppopt usage only.
     */
    virtual void m_internal_toggle(const std::vector<std::string>& params)
    {
        (void) params;

        ++(*m_counter_ref);
    }

private:
    /**
     * @brief Pointer to the counter to be incremented on toggle.
     */
    CounterType* m_counter_ref;
};

} /* namespace cppopt */

#endif /* CPPOPT_INCREMENTER_HPP */
