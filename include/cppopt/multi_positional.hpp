/**
 * @file   multi_positional.hpp
 * @author John Wikman
 * @brief  Implements positionals that can be toggled multiple times.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_MULTI_POSITIONAL_HPP
#define CPPOPT_MULTI_POSITIONAL_HPP

#include <array>
#include <memory>
#include <string>
#include <vector>

#include "cast_optval.hpp"
#include "exception.hpp"
#include "modifier.hpp"
#include "positional_base.hpp"

namespace cppopt {

/**
 * @brief A multi-valued positional that can be toggled multiple times.
 *
 * Example usage:
 * @code{.cpp}
 * // individual vectors
 * std::vector<std::string> ftarget;
 * std::vector<int> itarget;
 * auto vec_parser = cppopt::parser(...);
 * vec_parser.add(
 *     cppopt::multi_positional<std::string, int>(
 *         {"FILE", "OFFSET"}, "Files and offsets to use",
 *         &ftarget, &itarget, cppopt::OPTIONAL
 *     )
 * );
 * vec_parser.parse({"prog"})                      // ftarget == {},         itarget == {}
 * vec_parser.parse({"prog", "X", "55"})           // ftarget == {"X"},      itarget == {55}
 * vec_parser.parse({"prog", "X", "55", "Y", "3"}) // ftarget == {"X", "Y"}, itarget == {55, 3}
 *
 * // single tuple vector
 * std::vector<std::tuple<std::string,int>> target;
 * auto tup_parser = cppopt::parser(...);
 * tup_parser.add(
 *     cppopt::multi_positional<std::string, int>(
 *         {"FILE", "OFFSET"}, "Files and offsets to use (tuple mode)",
 *         &target, cppopt::OPTIONAL
 *     )
 * );
 * tup_parser.parse({"prog"})                      // target == {}
 * tup_parser.parse({"prog", "X", "55"})           // target == {("X", 55)}
 * tup_parser.parse({"prog", "X", "55", "Y", "3"}) // target == {("X", 55), ("Y", 3)}
 *
 * @endcode
 */
template <typename... ArgTypes>
class multi_positional: public positional_base {
    static_assert(sizeof...(ArgTypes) > 0, "A multi_positional must have at least one type argument.");
private:
    /**
     * @brief Internal enum used to determine how the values are referenced.
     */
    enum class refmode {TUPLE, INDIVIDUAL_VECTORS};
public:
    /**
     * @brief Constructor for a multi_positional. (Tuple version)
     * @param metavars The metavars for this multi_positional. Number of
     *                 metavars must match the number of types.
     * @param description Description shown when printing the help message.
     * @param value_refs The vector where to store tuples of parsed values.
     * @param mod Modifiers to be applied to this positional. See
     *            cppopt::positional_base for the effects that specific
     *            modifiers have.
     */
    multi_positional(const std::array<std::string, sizeof...(ArgTypes)>& metavars,
                     const std::string& description,
                     std::vector<std::tuple<ArgTypes...>>* value_refs,
                     modifier mod = REQUIRED):
        positional_base(std::vector<std::string>(metavars.begin(), metavars.end()),
                        description,
                        mod,
                        true),
        m_refmode(multi_positional::refmode::TUPLE),
        m_tuple_value_refs(value_refs),
        m_individual_vector_value_refs()
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for a multi_positional. (Separate vector version)
     * @param metavars The metavars for this multi_positional. Number of
     *                 metavars must match the number of types.
     * @param description Description shown when printing the help message.
     * @param value_refs The vectors where to store individual parsed values.
     * @param mod Modifiers to be applied to this positional. See
     *            cppopt::positional_base for the effects that specific
     *            modifiers have.
     */
    multi_positional(const std::array<std::string, sizeof...(ArgTypes)>& metavars,
                     const std::string& description,
                     std::vector<ArgTypes>* ...value_refs,
                     modifier mod = REQUIRED):
        positional_base(std::vector<std::string>(metavars.begin(), metavars.end()),
                        description,
                        mod,
                        true),
        m_refmode(multi_positional::refmode::INDIVIDUAL_VECTORS),
        m_tuple_value_refs(nullptr),
        m_individual_vector_value_refs(value_refs...)
    {
        // Do nothing here
    }

    /**
     * @brief Destructor.
     */
    virtual ~multi_positional() {}

    /**
     * @brief Copy-constructor.
     * @param other The multi_positional object to copy from.
     */
    multi_positional(const multi_positional& other):
        positional_base(other),
        m_refmode(other.m_refmode),
        m_tuple_value_refs(other.m_tuple_value_refs),
        m_individual_vector_value_refs(other.m_individual_vector_value_refs)
    {
        // Do nothing here
    }

    /**
     * @brief Move-constructor.
     * @param other The multi_positional object to move into this object.
     */
    multi_positional(multi_positional&& other) noexcept: positional_base(other)
    {
        std::swap(m_refmode, other.m_refmode);
        std::swap(m_tuple_value_refs, other.m_tuple_value_refs);
        std::swap(m_individual_vector_value_refs, other.m_individual_vector_value_refs);
    }

    /**
     * @brief Copy-assignment.
     * @param other The multi_positional object to copy from.
     * @return A reference to this multi_positional object after the copy.
     */
    multi_positional& operator=(const multi_positional& other)
    {
        positional_base::operator=(other);
        m_refmode = other.m_refmode;
        m_tuple_value_refs = other.m_tuple_value_refs;
        m_individual_vector_value_refs = other.m_individual_vector_value_refs;
        return *this;
    }

    /**
     * @brief Move-assignment.
     * @param other The multi_positional object to move into this object.
     * @return A reference to this multi_positional object after the move.
     */
    multi_positional& operator=(multi_positional&& other) noexcept
    {
        positional_base::operator=(other);
        std::swap(m_refmode, other.m_refmode);
        std::swap(m_tuple_value_refs, other.m_tuple_value_refs);
        std::swap(m_individual_vector_value_refs, other.m_individual_vector_value_refs);
        return *this;
    }

    /**
     * @brief Returns a dynamically allocated copy of the multi_positional
     *        object.
     * @return An std::shared_ptr containing a dynamically allocated pointer to
     *         a cppopt::multi_positional object.
     */
    std::shared_ptr<positional_base> copy() const override
    {
        return std::shared_ptr<positional_base>(new multi_positional<ArgTypes...>(*this));
    }

private:
    /**
     * @brief Base case for individual vector partial toggle.
     * @param args The argument vector passed to initial invocation of toggle.
     * @note Only used if specified multi_positional with individual vectors
     *       for each value.
     */
    template <typename TLast>
    void m_vector_internal_partial_toggle(const std::vector<std::string>& args)
    {
        constexpr std::size_t idx = (sizeof...(ArgTypes)) - 1;

        // Apply last parameter
        m_vector_cast_and_assign<TLast, idx>(args[idx]);
    }

    /**
     * @brief Partial toggle on a single argument when using individual vector
     *        pointers. Recursively invoked.
     * @param args The argument vector passed to initial invocation of toggle.
     * @note Only used if specified multi_positional with individual vectors
     *       for each value.
     *
     * Apply one argument, and then recurse until base case is reached.
     */
    template <typename TFirst, typename TNext, typename... TRest>
    void m_vector_internal_partial_toggle(const std::vector<std::string>& args)
    {
        constexpr std::size_t idx = (sizeof...(ArgTypes)) - (sizeof...(TRest)) - 2;

        // Apply first parameter
        m_vector_cast_and_assign<TFirst, idx>(args[idx]);

        // Recurse with remaining parameters
        m_vector_internal_partial_toggle<TNext, TRest...>(args);
    }

    /**
     * @brief Cast and assign the positional argument into its corresponding
     *        vector.
     * @param arg The value to cast and assign.
     * @note Only used if specified multi_positional with individual vectors
     *       for each parameter.
     *
     * @todo Introduce a new exception here that also specifies the parameter
     *       index.
     */
    template <typename ArgType, std::size_t idx>
    void m_vector_cast_and_assign(const std::string& arg)
    {
        std::vector<ArgType>* ref = std::get<idx>(m_individual_vector_value_refs);

        ref->push_back(cast_optval<ArgType>(arg));
    }

    /**
     * @brief Base case for tuple vector partial toggle.
     * @param args The argument vector passed to initial invocation of toggle.
     * @return Single-value tuple with the result of the last argument.
     * @note Only used if specified multi_positional with a tuple vector.
     */
    template <typename TLast>
    std::tuple<TLast> m_tuple_internal_partial_toggle(const std::vector<std::string>& args)
    {
        constexpr std::size_t idx = (sizeof...(ArgTypes)) - 1;

        // Convert last parameter
        return std::make_tuple(cast_optval<TLast>(args[idx]));
    }

    /**
     * @brief Partial toggle on a single argument when using a tuple vector
     *        pointer. Recursively invoked.
     * @param args The argument vector passed to initial invocation of toggle.
     * @return Tuple with the partial results.
     * @note Only used if specified multi_positional with tuples.
     *
     * Apply one parameter, and then recurse until base case is reached.
     */
    template <typename TFirst, typename TNext, typename... TRest>
    std::tuple<TFirst, TNext, TRest...> m_tuple_internal_partial_toggle(const std::vector<std::string>& args)
    {
        constexpr std::size_t idx = (sizeof...(ArgTypes)) - (sizeof...(TRest)) - 2;

        // Convert first parameter
        std::tuple<TFirst> t_head = std::make_tuple(cast_optval<TFirst>(args[idx]));

        // Fetch the other parameters.
        std::tuple<TNext, TRest...> t_tail = m_tuple_internal_partial_toggle<TNext, TRest...>(args);

        return std::tuple_cat(t_head, t_tail);
    }

protected:
    /**
     * @brief Internal toggle specific to the multi_positional class.
     * @param args Arguments for this positional.
     *
     * @note Internal cppopt usage only.
     */
    void m_internal_toggle(const std::vector<std::string>& args) override
    {
        switch (m_refmode) {
        case multi_positional::refmode::TUPLE:
            m_tuple_value_refs->push_back(m_tuple_internal_partial_toggle<ArgTypes...>(args));
            break;
        case multi_positional::refmode::INDIVIDUAL_VECTORS:
            m_vector_internal_partial_toggle<ArgTypes...>(args);
            break;
        }
    }

private:
    /**
     * @brief The reference mode being used.
     */
    multi_positional::refmode m_refmode;

    /**
     * @brief Storage location in terms of a vector of tuples.
     */
    std::vector<std::tuple<ArgTypes...>>* m_tuple_value_refs;

    /**
     * @brief Storage locations in terms of individual vectors for each type.
     */
    std::tuple<std::vector<ArgTypes>*...> m_individual_vector_value_refs;
};

} /* namespace cppopt */

#endif /* CPPOPT_MULTI_POSITIONAL_HPP */
