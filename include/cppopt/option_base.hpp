/**
 * @file   option_base.hpp
 * @author John Wikman
 * @brief  Abstract option superclass definition.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_OPTION_BASE_HPP
#define CPPOPT_OPTION_BASE_HPP

#include <cstddef>
#include <cstdint>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include "modifier.hpp"

namespace cppopt {

/**
 * @brief Base class for all other options. Never directly instantiated.
 */
class option_base {
public:
    virtual ~option_base();

    virtual std::shared_ptr<option_base> copy() const;

    void toggle(const std::vector<std::string>& params);
    void set_enums(const std::vector<std::set<std::string>>& enum_values);

    bool has_short() const;
    char shortname() const;
    const std::string& longname() const;
    const std::string& description() const;
    const std::vector<std::string>& metavars() const;
    const std::vector<std::set<std::string>>& enums() const;
    std::size_t param_count() const;
    modifier modifiers() const;

    bool toggled() const;
    std::size_t toggle_count() const;
    bool required() const;

protected:
    // This class should never be instantiated directly.
    option_base(char shortopt, const std::string& longopt,
                const std::vector<std::string>& metavars,
                const std::string& description,
                modifier mod = NONE);
    option_base(const std::string& longopt,
                const std::vector<std::string>& metavars,
                const std::string& description,
                modifier mod = NONE);

    option_base(const option_base& other);
    option_base(option_base&& other) noexcept;

    option_base& operator=(const option_base& other);
    option_base& operator=(option_base&& other) noexcept;

    virtual void m_internal_toggle(const std::vector<std::string>& params);

private:
    /**
     * @brief The short "single-character" identifier for this option.
     * @note m_short == '\0' indicates the absence of a short identifier.
     */
    char m_short;

    /**
     * @brief The long "full-name" identifier for this option.
     */
    std::string m_long;

    /**
     * @brief Description of what this option does.
     */
    std::string m_description;

    /**
     * @brief Modifiers that determine option behavior.
     */
    modifier m_modifiers;

    /**
     * @brief The meta variables for this option.
     *
     * The size of this vector determines how many parameters the option
     * accepts. E.g. for an option "-d":
     * Example 1: m_metavars.size() == 0  =>  `-d` (no parameters)
     * Example 2: m_metavars.size() == 1  =>  `-d foo` (1 parameter)
     * Example 3: m_metavars.size() == 2  =>  `-d bar 2.5` (2 parameters)
     * etc...
     */
    std::vector<std::string> m_metavars;

    /**
     * @brief Enumerated values for each of the parameters.
     * @note Empty set indicates no enumeration.
     */
    std::vector<std::set<std::string>> m_enum_values;

    /**
     * @brief A counter over number of times this option has been toggled.
     */
    std::uint32_t m_times_toggled;
};

} /* namespace cppopt */

#endif /* CPPOPT_OPTION_BASE_HPP */
