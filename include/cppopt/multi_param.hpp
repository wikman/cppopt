/**
 * @file    multi_param.hpp
 * @author  John Wikman
 * @brief   Defines a parameter that can be toggled multiple times.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_MULTI_PARAM_HPP
#define CPPOPT_MULTI_PARAM_HPP

#include <array>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "cast_optval.hpp"
#include "exception.hpp"
#include "option_base.hpp"

namespace cppopt {

/**
 * @brief A parameter option that can be toggled multiple times and records the
 *        values from all toggles.
 *
 * While the regular cppopt::param class can also be toggled multiple times,
 * the cppopt::multi_param will record the inputs from all toggles of it
 * whereas the cppopt::param will only record input from the latest toggle.
 *
 * Example usage:
 * @code{.cpp}
 * std::vector<int> numbers;
 * std::vector<std::tuple<std::string,int>> numpairs;
 * std::vector<int> vec_a;
 * std::vector<int> vec_b;
 *
 * auto parser = cppopt::parser(...);
 * parser.add(
 *     cppopt::multi_param<int>('n', "number", "Specify number", &numbers),
 *     cppopt::multi_param<std::string, int>('p', "pair", "Specify name-number pair", &numpairs),
 *     cppopt::multi_param<int, int>("sep", "Insert numbers into separate vectors", &vec_a, &vec_b)
 * );
 *
 * ...
 *
 * // For `numbers`
 * parser.parse({"prog"});                         // numbers == {}
 * parser.parse({"prog", "-n", "2"});              // numbers == {2}
 * parser.parse({"prog", "-n2", "--number", "8"}); // numbers == {2, 8}
 *
 * // For `numpairs`
 * parser.parse({"prog"});                  // numpairs == {}
 * parser.parse({"prog", "-p", "IX", "9"}); // numpairs == {("IX", 9)}
 *
 * // numpairs == {("IX", 9), ("THIrTY", 30)}
 * parser.parse({"prog", "-pIX", "9", "--pair", "THIrTY", "30"});
 *
 * // For `vec_a` and `vec_b`
 * parser.parse({"prog"});                    // vec_a == {},  vec_b == {}
 * parser.parse({"prog", "--sep", "2", "3"}); // vec_a == {2}, vec_b == {3}
 *
 * // vec_a == {2, 5}, vec_b == {3, 7}
 * parser.parse({"prog", "--sep", "2", "3", "--sep", "5", "7"});
 * @endcode
 */
template <typename... ParamTypes>
class multi_param: public option_base {
    static_assert(sizeof...(ParamTypes) > 0, "A multi_param option must have at least one type argument.");
private:
    /**
     * @brief Internal enum used to determine how the values are referenced.
     */
    enum class refmode {TUPLE, INDIVIDUAL_VECTORS};
public:
    /**
     * @brief Constructor for multi_param option including a short name.
     *        (Tuple version)
     * @param shortopt The short name of this option.
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param value_refs Pointer to a vector where tuples of the toggled
     *                   parameters will be inserted.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    multi_param(char shortopt, const std::string& longopt,
                const std::array<std::string, sizeof...(ParamTypes)>& metavars,
                const std::string& description,
                std::vector<std::tuple<ParamTypes...>>* value_refs,
                modifier mod = NONE):
        option_base(shortopt,
                    longopt,
                    std::vector<std::string>(metavars.begin(), metavars.end()),
                    description,
                    mod),
        m_refmode(multi_param::refmode::TUPLE),
        m_individual_vector_value_refs(),
        m_tuple_value_refs(value_refs)
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for multi_param option including a short name.
     *        (Separate vector version)
     * @param shortopt The short name of this option.
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param value_refs Pointers to the individual vectors where the toggled
     *                   parameters will be inserted.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    multi_param(char shortopt, const std::string& longopt,
                const std::array<std::string, sizeof...(ParamTypes)>& metavars,
                const std::string& description,
                std::vector<ParamTypes>* ...value_refs,
                modifier mod = NONE):
        option_base(shortopt,
                    longopt,
                    std::vector<std::string>(metavars.begin(), metavars.end()),
                    description,
                    mod),
        m_refmode(multi_param::refmode::INDIVIDUAL_VECTORS),
        m_individual_vector_value_refs(value_refs...),
        m_tuple_value_refs(nullptr)
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for multi_param option with only a long name.
     *        (Tuple version)
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param value_refs Pointer to a vector where tuples of the toggled
     *                   parameters will be inserted.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    multi_param(const std::string& longopt,
                const std::array<std::string, sizeof...(ParamTypes)>& metavars,
                const std::string& description,
                std::vector<std::tuple<ParamTypes...>>* value_refs,
                modifier mod = NONE):
        multi_param('\0',
                    longopt,
                    metavars,
                    description,
                    value_refs,
                    mod)
    {
        // Do nothing here
    }

    /**
     * @brief Constructor for multi_param option with only a long name.
     *        (Separate vector version)
     * @param longopt The long name of this option.
     * @param metavars The display name of the parameters to this option.
     * @param description Description shown when printing the help message.
     * @param value_refs Pointers to the individual vectors where the toggled
     *                   parameters will be inserted.
     * @param mod Modifiers to be applied to this option. See
     *            cppopt::option_base for the effects that specific modifiers
     *            have.
     */
    multi_param(const std::string& longopt,
                const std::array<std::string, sizeof...(ParamTypes)>& metavars,
                const std::string& description,
                std::vector<ParamTypes>* ...value_refs,
                modifier mod = NONE):
        multi_param('\0',
                    longopt,
                    metavars,
                    description,
                    value_refs...,
                    mod)
    {
        // Do nothing here
    }

    /**
     * @brief Destructor.
     */
    virtual ~multi_param() {}

    /**
     * @brief Copy-constructor.
     * @param other The multi_param object to copy from.
     */
    multi_param(const multi_param& other):
        option_base(other),
        m_refmode(other.m_refmode),
        m_individual_vector_value_refs(other.m_individual_vector_value_refs),
        m_tuple_value_refs(other.m_tuple_value_refs)
    {
        // Do nothing here
    }

    /**
     * @brief Move-constructor.
     * @param other The multi_param object to move into this object.
     */
    multi_param(multi_param&& other) noexcept: option_base(other)
    {
        std::swap(m_refmode, other.m_refmode);
        std::swap(m_individual_vector_value_refs, other.m_individual_vector_value_refs);
        std::swap(m_tuple_value_refs, other.m_tuple_value_refs);
    }

    /**
     * @brief Copy-assignment.
     * @param other The multi_param object to copy from.
     * @return A reference to this multi_param object after the copy operation.
     */
    multi_param& operator=(const multi_param& other)
    {
        option_base::operator=(other);
        m_refmode = other.m_refmode;
        m_individual_vector_value_refs = other.m_individual_vector_value_refs;
        m_tuple_value_refs = other.m_tuple_value_refs;
        return *this;
    }

    /**
     * @brief Move-assignment.
     * @param other The multi_param object to move into this object.
     * @return A reference to this multi_param object after the move.
     */
    multi_param& operator=(multi_param&& other) noexcept
    {
        option_base::operator=(other);
        std::swap(m_refmode, other.m_refmode);
        std::swap(m_individual_vector_value_refs, other.m_individual_vector_value_refs);
        std::swap(m_tuple_value_refs, other.m_tuple_value_refs);
        return *this;
    }

    /**
     * @brief Returns a dynamically allocated copy of the multi_param object.
     * @return An std::shared_ptr containing a dynamically allocated pointer to
     *         a cppopt::multi_param object.
     */
    std::shared_ptr<option_base> copy() const override
    {
        return std::shared_ptr<option_base>(new multi_param<ParamTypes...>(*this));
    }

private:
    /**
     * @brief Base case for individual vector partial toggle.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @note Only used if specified multi_param with individual vectors for
     *       each parameter.
     */
    template <typename TLast>
    void m_individual_vector_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - 1;

        // Apply last parameter
        m_individual_vector_cast_and_assign<TLast, idx>(params[idx]);
    }

    /**
     * @brief Partial toggle on a single argument when using individual vector
     *        pointers. Recursively invoked.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @note Only used if specified multi_param with individual vectors for
     *       each parameter.
     *
     * Apply one parameter, and then recurse until base case is reached.
     */
    template <typename TFirst, typename TNext, typename... TRest>
    void m_individual_vector_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - (sizeof...(TRest)) - 2;

        // Apply first parameter
        m_individual_vector_cast_and_assign<TFirst, idx>(params[idx]);

        // Recurse with remaining parameters
        m_individual_vector_internal_partial_toggle<TNext, TRest...>(params);
    }

    /**
     * @brief Cast and assign the parameterized value into its corresponding
     *        vector.
     * @param param The value to cast and assign.
     * @note Only used if specified multi_param with individual vectors for
     *       each parameter.
     *
     * @todo Introduce a new exception here that also specifies the parameter
     *       index.
     */
    template <typename ParamType, std::size_t idx>
    void m_individual_vector_cast_and_assign(const std::string& param)
    {
        std::vector<ParamType>* ref = std::get<idx>(m_individual_vector_value_refs);

        ref->push_back(cast_optval<ParamType>(param));
    }

    /**
     * @brief Base case for tuple vector partial toggle.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @return Single-value tuple with the result of the last parameter.
     * @note Only used if specified multi_param with a tuple vector.
     */
    template <typename TLast>
    std::tuple<TLast> m_tuple_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - 1;

        // Convert last parameter
        return std::make_tuple(cast_optval<TLast>(params[idx]));
    }

    /**
     * @brief Partial toggle on a single argument when using a tuple vector
     *        pointer. Recursively invoked.
     * @param params The parameter vector passed to initial invocation of
     *               toggle.
     * @return Tuple with the partial results.
     * @note Only used if specified multi_param with a tuple vector.
     *
     * Apply one parameter, and then recurse until base case is reached.
     */
    template <typename TFirst, typename TNext, typename... TRest>
    std::tuple<TFirst, TNext, TRest...> m_tuple_internal_partial_toggle(const std::vector<std::string>& params)
    {
        constexpr std::size_t idx = (sizeof...(ParamTypes)) - (sizeof...(TRest)) - 2;

        // Convert first parameter
        std::tuple<TFirst> t_head = std::make_tuple(cast_optval<TFirst>(params[idx]));

        // Fetch the other parameters.
        std::tuple<TNext, TRest...> t_tail = m_tuple_internal_partial_toggle<TNext, TRest...>(params);

        return std::tuple_cat(t_head, t_tail);
    }

protected:
    /**
     * @brief Internal toggle specific to the multi_param class.
     * @param params Parameters for this option.
     *
     * @note Internal cppopt usage only.
     */
    virtual void m_internal_toggle(const std::vector<std::string>& params)
    {
        switch (m_refmode) {
        case multi_param::refmode::TUPLE:
            m_tuple_value_refs->push_back(m_tuple_internal_partial_toggle<ParamTypes...>(params));
            break;
        case multi_param::refmode::INDIVIDUAL_VECTORS:
            m_individual_vector_internal_partial_toggle<ParamTypes...>(params);
            break;
        }
    }

private:
    /**
     * @brief Indicates which reference mode to use.
     */
    multi_param::refmode m_refmode;

    /**
     * @brief Individual vector pointers for where to store parameter values.
     * @note Only used if m_refmode is INDIVIDUAL_VECTORS.
     */
    std::tuple<std::vector<ParamTypes>*...> m_individual_vector_value_refs;

    /**
     * @brief Tuple vector pointer for where to store parameter values.
     * @note Only used if m_refmode is TUPLE.
     */
    std::vector<std::tuple<ParamTypes...>>* m_tuple_value_refs;
};

} /* namespace cppopt */

#endif /* CPPOPT_MULTI_PARAM_HPP */
