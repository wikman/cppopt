/**
 * @file   parser.hpp
 * @author John Wikman
 * @brief  Input parser class definition.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_PARSER_HPP
#define CPPOPT_PARSER_HPP

#include <functional>
#include <initializer_list>
#include <memory>
#include <ostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "help_printer.hpp"
#include "naming.hpp"
#include "option_base.hpp"
#include "positional_base.hpp"

namespace cppopt {

/**
 * @brief Parser and collection of input arguments.
 *
 * Add optional and positional arguments to the parser through the add()
 * function. The order in which positional arguments are provided is the same
 * order as they will be provided in the program invocation. The order in
 * which optional arguments are provided have no practical effect on the parser
 * behavior.
 *
 * A parser also comes with a built-in help option. See the constructor for
 * more details.
 *
 * @see add
 * @see parse
 */
class parser {
public:
    parser(const std::string& description = "",
           char help_shortopt = 'h',
           const std::string& help_longopt = "help");
    virtual ~parser();

    parser(const parser& other);
    parser(parser&& other) noexcept;

    parser& operator=(const parser& other);
    parser& operator=(parser&& other) noexcept;

    void parse(int argc, char** argv);
    void parse(int argc, const char** argv);
    void parse(const std::vector<std::string>& argv);

    void error(const std::string& msg);

    void set_optterm_token(const std::string& token);
    void set_option_naming_rule(option_naming_rule rule,
                                bool error_on_present_invalids = true);
    void exit_on_error(bool eoe = true);
private:
    std::size_t parse_option(const std::vector<std::string>& argv,
                             std::size_t offset);
    std::size_t parse_option_parameters(const std::vector<std::string>& argv,
                                        std::size_t offset,
                                        std::shared_ptr<option_base> opt,
                                        const std::string& errname,
                                        const std::string& compount_param);
    void add_individual(const option_base& opt);
    void add_individual(const positional_base& pos);

    /**
     * @brief Base case for internal add.
     * @param arg The argument to add. The type of this argument must have a
     *            matching definition for add_individual().
     */
    template<typename TLast>
    void m_internal_add(const TLast& arg)
    {
        add_individual(arg);
    }

    /**
     * @brief Recursive case for internal add with at least 2 arguments.
     * @param first The argument that will be added in this iteration.
     * @param next The argument to be added in the next iteration.
     * @param rest Any additional arguments that will be added after the `next`
     *             argument.
     *
     * @note All argument types here must have a matching definition for
     *       add_individual().
     *
     * Will recursively invoke itself with one less argument until the base
     * case is reached.
     */
    template<typename TFirst, typename TNext, typename... TRest>
    void m_internal_add(const TFirst& first, const TNext& next, const TRest& ...rest)
    {
        add_individual(first);
        m_internal_add<TNext, TRest...>(next, rest...);
    }
public:
    /**
     * @brief Adds provided positionals and options to the parser.
     * @param args The arguments that will be added to the parser. Accepts an
     *             arbitrary number of arguments. The type of these arguments
     *             must either extend cppopt::positional_base or
     *             cppopt::option_base.
     *
     * If adding a positional argument, the type of the provided value must be
     * a subclass of cppopt::positional_base. The order in which the positional
     * arguments are specified in args is the same order that they will be
     * parsed in when calling parse().
     *
     * The parsing of positional arguments is a completely deterministic
     * process where it is always known what the next positional argument to
     * parse will be. As such, it is not possible to specify positional
     * arguments in a way where it is ambiguous to which positional a new input
     * argument should be added to. An example of ambiguity is adding a
     * positional argument after a cppopt::multi_positional argument.
     *
     * Example of adding positional argument:
     * @code{.cpp}
     * std::string a;
     * std::string b;
     * auto pos_a = cppopt::positional<std::string>("VALUE_A", "pos_a descr.", &a);
     * auto pos_b = cppopt::positional<std::string>("VALUE_B", "pos_b descr.", &b);
     * auto parser = cppopt::parser();
     * parser.add(pos_a, pos_b);
     * parser.parse("prog", "FOO", "Bar");
     * // a == "FOO"
     * // b == "Bar"
     * @endcode
     *
     * If adding an optional argument, the type of the provided value must be a
     * subclass of cppopt::option_base. The order in which the optional
     * arguments are specified in args has no practical effect on the parsing.
     * However, two positional arguments cannot share either a long name or a
     * short name. The long name and the short name can also not with the
     * naming rule set by set_option_naming_rule().
     *
     * Example of adding optional argument:
     * @code{.cpp}
     * std::string a;
     * std::string b;
     * auto opt_a = cppopt::param<std::string>('a', "av", {"VALUE_A"}, "opt_a descr.", &a);
     * auto opt_b = cppopt::param<std::string>('b', "bv", {"VALUE_B"}, "opt_b descr.", &b);
     * auto parser = cppopt::parser();
     * parser.add(opt_a, opt_b);
     * parser.parse("prog", "-b=4c", "-a=2p");
     * // a == "2p"
     * // b == "4c"
     * @endcode
     *
     * @throws std::invalid_argument If something is wrong with one of the
     *                               provided arguments, or if something is
     *                               wrong with the order in which arguments
     *                               were added.
     *
     * @see cppopt::positional_base
     * @see cppopt::option_base
     */
    template<typename... ArgTypes>
    void add(const ArgTypes& ...args)
    {
        m_internal_add<ArgTypes...>(args...);
    }
private:
    /**
     * @brief Program description to be shown on help.
     */
    std::string m_description;

    /**
     * @brief List of all the positionals added to this parser.
     */
    std::vector<std::shared_ptr<positional_base>> m_positionals;

    /**
     * @brief List of all the options added to this parser.
     * @warning Parser data structure relies on that no intermediate deletions
     *          are made and that all options are inserted at the end of the
     *          vector.
     */
    std::vector<std::shared_ptr<option_base>> m_options;

    /**
     * @brief Map from shortopt -> index in `m_opts` for corresponding option.
     */
    std::unordered_map<char, int> m_shortmap;

    /**
     * @brief Map from longopt -> index in `m_opts` for corresponding option.
     */
    std::unordered_map<std::string, int> m_longmap;

    /**
     * @brief Token that indicates that all further arguments should be parsed
     *        as positionals.
     */
    std::string m_optterm_token;

    /**
     * @brief Whether or not option termination is enabled.
     */
    bool m_optterm_enabled;

    /**
     * @brief The naming rule used used for optional arguments.
     */
    option_naming_rule m_opt_rule;

    /**
     * @brief Boolean flag that indicates whether the help option has been triggered.
     */
    bool m_help_triggered;

    /**
     * @brief The long option name for printing out the help message.
     */
    std::string m_help_longname;

    /**
     * @brief Whether or not to exit on error.
     */
    bool m_exit_on_error;

    /**
     * @brief Internal help_printer.
     */
    help_printer m_help_printer;
};

} /* namespace cppopt */

#endif /* CPPOPT_PARSER_HPP */
