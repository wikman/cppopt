/**
 * @file    help_printer.hpp
 * @author  John Wikman
 * @brief Defines the help_printer class. Internal class for printing help
 *        messages.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_HELP_PRINTER_HPP
#define CPPOPT_HELP_PRINTER_HPP

#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#include "option_base.hpp"
#include "positional_base.hpp"

namespace cppopt {

/**
 * @brief Internal class for printing help messages.
 *
 * Moved to a stand-alone class to avoid cluttering up cppopt::parser.
 */
class help_printer {
public:
    help_printer(const std::string& progname = "<cmd>");
    virtual ~help_printer();

    help_printer(const help_printer& other);
    help_printer(help_printer&& other) noexcept;

    help_printer &operator=(const help_printer& other);
    help_printer &operator=(help_printer&& other) noexcept;

    void set_progname(const std::string& progname);
    void set_description(const std::string& description);
    void set_optterm_token(const std::string& token);
    void set_fixed_maxwidth(std::size_t maxwidth);

    void set_options(const std::vector<std::shared_ptr<option_base>>& optvec);
    void set_positionals(const std::vector<std::shared_ptr<positional_base>>& posvec);

    void print_full(std::ostream& os, std::size_t interleaved_lines = 1) const;
    void print_string(std::ostream& os, const std::string& s) const;

    void print_usage(std::ostream& os) const;
    void print_description(std::ostream& os) const;
    void print_positional_details(std::ostream& os) const;
    void print_optional_details(std::ostream& os) const;

private:
    std::size_t get_maxwidth() const;

    // Internal helper functions for print formatting
    void print_string_indent_subseq_lines(std::ostream& os,
                                          const std::vector<std::string>& strs,
                                          const std::vector<bool>& enable_reformatting,
                                          std::size_t indent,
                                          bool fill_linewidth) const;
    void indent_and_print_line(std::ostream& os,
                               const std::vector<std::string>& words,
                               std::size_t indent,
                               bool fill_linewidth) const;

    /**
     * @brief The program name to be shown in the usage print.
     */
    std::string m_progname;

    /**
     * @brief Longer program description.
     */
    std::string m_description;

    /**
     * @brief Vector of all positional arguments that shall be included in
     *        usage prints and detailed prints.
     */
    std::vector<std::shared_ptr<positional_base>> m_positionals;

    /**
     * @brief Vector of all optional arguments that shall be included in usage
     *        prints and detailed prints.
     */
    std::vector<std::shared_ptr<option_base>> m_options;

    /**
     * @brief The option termination token.
     */
    std::string m_optterm_token;

    /**
     * @brief Whether or not an option termination token is used.
     */
    bool m_has_optterm_token;

    /**
     * @brief Whether or not to used a fixed maximum line width when printing
     *        help messages.
     *
     * @todo This should probably be replaced with an enumerated value when
     *       support for different line width modes are added.
     */
    bool m_use_fixed_maxwidth;

    /**
     * @brief The maximum line width value to be used if fixed maximum width is
     *        specified.
     */
    std::size_t m_fixed_maxwidth;
};

} /* namespace cppopt */

#endif /* CPPOPT_HELP_PRINTER_HPP */
