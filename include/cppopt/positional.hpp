/**
 * @file   positional.hpp
 * @author John Wikman
 * @brief  Implements positional argument functionality.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_POSITIONAL_HPP
#define CPPOPT_POSITIONAL_HPP

#include <memory>
#include <string>
#include <vector>

#include "cast_optval.hpp"
#include "exception.hpp"
#include "modifier.hpp"
#include "positional_base.hpp"

namespace cppopt {

/**
 * @brief A positional argument that accepts a single value and can be toggled
 *        a single time.
 *
 * Example usage:
 * @code{.cpp}
 * int i = 40;
 * std::string s = "init";
 *
 * auto parser = cppopt::parser();
 * parser.add(
 *     cppopt::positional<int>("INTVAL", "Sets the i variable.", &i),
 *     cppopt::positional<std::string>("STRVAL", "Sets the s variable.", &s, cppopt::OPTIONAL)
 * );
 * parser.parse({"prog", "5"})           // i == 5,  s == "init"
 * parser.parse({"prog", "0x10", "foo"}) // i == 16, s == "foo"
 * @endcode
 */
template <typename ArgType>
class positional: public positional_base {
public:
    /**
     * @brief Constructor for a positional.
     * @param metavar The metavar for this positional to be displayed when
     *                printing the help message.
     * @param description Description to be shown when printing the help
     *                    message.
     * @param value_ref Pointer to the value to be set on toggle.
     * @param mod Modifiers to be applied to this positional. See
     *            cppopt::positional_base for the effects that specific
     *            modifiers have.
     */
    positional(const std::string& metavar,
               const std::string& description,
               ArgType* value_ref,
               modifier mod = REQUIRED):
        positional_base({metavar}, description, mod, false),
        m_value_ref(value_ref)
    {
        // Do nothing here
    }

    /**
     * @brief Destructor.
     */
    virtual ~positional() {}

    /**
     * @brief Copy-constructor.
     * @param other The positional object to copy from.
     */
    positional(const positional& other):
        positional_base(other),
        m_value_ref(other.m_value_ref)
    {
        // Do nothing here
    }

    /**
     * @brief Move-constructor.
     * @param other The positional object to move into this object.
     */
    positional(positional&& other) noexcept: positional_base(other)
    {
        std::swap(m_value_ref, other.m_value_ref);
    }

    /**
     * @brief Copy-assignment.
     * @param other The positional object to copy from.
     * @return A reference to this object after the copy operation.
     */
    positional& operator=(const positional& other)
    {
        positional_base::operator=(other);
        m_value_ref = other.m_value_ref;
        return *this;
    }

    /**
     * @brief Move-assignment.
     * @param other The positional object to move into this object.
     * @return A reference to this object after the move operation.
     */
    positional& operator=(positional&& other) noexcept
    {
        positional_base::operator=(other);
        std::swap(m_value_ref, other.m_value_ref);
        return *this;
    }

    /**
     * @brief Returns a dynamically allocated copy of the positional object.
     * @return An std::shared_ptr containing a dynamically allocated pointer to a
     *         cppopt::positional object.
     */
    std::shared_ptr<positional_base> copy() const override
    {
        return std::shared_ptr<positional_base>(new positional<ArgType>(*this));
    }

protected:
    /**
     * @brief Internal toggle specific to the positional class.
     * @param args Positional arguments to toggle on.
     *
     * @note Internal cppopt usage only.
     *
     * @throws cppopt::invalid_usage If toggled more than once.
     */
    void m_internal_toggle(const std::vector<std::string>& args) override
    {
        if (this->toggle_count() != 0)
            throw cppopt::invalid_usage("cppopt::positional object can only be toggled at most once");

        *m_value_ref = cppopt::cast_optval<ArgType>(args[0]);
    }

private:
    /**
     * @brief Where the positional value shall be stored.
     */
    ArgType* m_value_ref;
};

} /* namespace cppopt */

#endif /* CPPOPT_POSITIONAL_HPP */
