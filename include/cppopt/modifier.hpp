/**
 * @file   modifier.hpp
 * @author John Wikman
 * @brief  Defines bitflag modifiers that can define behavior of cppopt
 *         objects.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_MODIFIER_HPP
#define CPPOPT_MODIFIER_HPP

#include <cstdint>

namespace cppopt {

/**
 * @brief Modifier type.
 *
 * Used as bitflag type. Combine modifiers by `MOD_A|MOD_B` to get a new
 * modifier with both MOD_A and MOD_B enabled.
 *
 * @note A light-weight type. Pass by value if applicable.
 */
typedef std::uint32_t modifier;

/**
 * @brief Base modifier with no flags activated.
 */
constexpr modifier NONE = 0x0;

/**
 * @brief Modifier specifying that something should be required.
 *
 * See documentation of the object where this is applied to for the effect that
 * this modifier flag will have.
 */
constexpr modifier REQUIRED = 0x1 << 0;

/**
 * @brief Alias for NONE. Provided to allow readability when explicitly
 *        specifying that something is not required.
 *
 * @note The result of `OPTIONAL|REQUIRED` will be `REQUIRED`.
 */
constexpr modifier OPTIONAL = NONE;

} /* namespace cppopt */

#endif /* CPPOPT_MODIFIER_HPP */
