/**
 * @file   positional_base.hpp
 * @author John Wikman
 * @brief  Abstract positional superclass definition.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_POSITIONAL_BASE_HPP
#define CPPOPT_POSITIONAL_BASE_HPP

#include <memory>
#include <set>
#include <string>
#include <vector>

#include "modifier.hpp"

namespace cppopt {

/**
 * @brief The base class for all positionals. Never directly instantiated.
 */
class positional_base {
public:
    virtual ~positional_base();

    virtual std::shared_ptr<positional_base> copy() const;

    void toggle(const std::vector<std::string>& args);
    void set_enums(const std::vector<std::set<std::string>>& enum_values);

    const std::string& description() const;
    const std::vector<std::string>& metavars() const;
    const std::vector<std::set<std::string>>& enums() const;
    modifier modifiers() const;

    bool toggled() const;
    std::size_t toggle_count() const;
    bool multi_togglable() const;
    bool optional() const;
    std::size_t arg_count() const;
protected:
    // Protected constructors.
    positional_base(const std::vector<std::string>& metavars,
                    const std::string& description,
                    modifier mod,
                    bool multiple_args);

    positional_base(const positional_base& other);
    positional_base(positional_base&& other) noexcept;

    positional_base& operator=(const positional_base& other);
    positional_base& operator=(positional_base&& other) noexcept;

    virtual void m_internal_toggle(const std::vector<std::string>& args);

private:
    /**
     * @brief Metavars for this positional.
     */
    std::vector<std::string> m_metavars;

    /**
     * @brief The description to be shown when printing the help message.
     */
    std::string m_description;

    /**
     * @brief Modifiers that determine positional behavior.
     */
    modifier m_modifiers;

    /**
     * @brief Enumerated values for each of the arguments.
     * @note Empty set indicates no enumeration.
     */
    std::vector<std::set<std::string>> m_enum_values;

    /**
     * @brief How many times the positional has been toggled.
     */
    std::size_t m_times_toggled;

    /**
     * @brief Whether or not this can be toggled multiple times.
     */
    bool m_accepts_multiple;
};

} /* namespace cppopt */

#endif /* CPPOPT_POSITIONAL_BASE_HPP */
