/**
 * @file   flag.hpp
 * @author John Wikman
 * @brief  Definition for a boolean flag option.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_FLAG_HPP
#define CPPOPT_FLAG_HPP

#include <memory>
#include <string>
#include <vector>

#include "modifier.hpp"
#include "option_base.hpp"

namespace cppopt {

/**
 * @brief A boolean flag option.
 *
 * Inverts the boolean value in the provided `value_ref` on toggle. Can be
 * toggled multiple times, but the value will only be inverted on the first
 * toggle. Subsequent toggles have no effect on value pointed to by
 * `value_ref`.
 *
 * Example usage:
 * @code{.cpp}
 * bool yes = false;
 * bool insert_into_db = true;
 *
 * auto parser = cppopt::parser(...);
 * parser.add(
 *     cppopt::flag('y', "yes", "Toggles the yes option", &yes),
 *     cppopt::flag("dry-run", "Disables DB insertion", &insert_into_db)
 * );
 *
 * ...
 *
 * // These will turn the `yes` variable to `true`
 * parser.parse({"prog", "-y"});
 * parser.parse({"prog", "--yes"});
 * parser.parse({"prog", "-y", "-y"});
 * parser.parse({"prog", "-y", "--yes"});
 *
 * // These will turn the `insert_into_db` variable to `false`
 * parser.parse({"prog", "--dry-run"});
 * parser.parse({"prog", "--dry-run", "--dry-run"});
 * @endcode
 */
class flag: public option_base {
public:
    flag(char shortopt, const std::string& longopt,
         const std::string& description,
         bool* value_ref,
         modifier mod = NONE);
    flag(const std::string& longopt,
         const std::string& description,
         bool* value_ref,
         modifier mod = NONE);

    virtual ~flag();

    flag(const flag& other);
    flag(flag&& other) noexcept;

    flag& operator=(const flag& other);
    flag& operator=(flag&& other) noexcept;

    std::shared_ptr<option_base> copy() const override;

protected:
    void m_internal_toggle(const std::vector<std::string>& params) override;

private:
    /**
     * @brief Pointer to the value to be updated on toggle.
     */
    bool* m_value_ref;
};

} /* namespace cppopt */

#endif /* CPPOPT_FLAG_HPP */
