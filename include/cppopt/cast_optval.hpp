/**
 * @file   cast_optval.hpp
 * @author John Wikman
 * @brief  Defines template and standard implementation for parameter type
 *         casting.
 *
 * cppopt::cast_optval should be defined for each type that shall be provided
 * parsed by cppopt. This library provides the basic set of implementations for
 * standard C++ types. Additional needed types must be defined by the user.
 *
 * On error, these functions must return cppopt::invalid_cast as that is how
 * the parser detects if a value is well formed or not.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_CAST_OPTVAL_HPP
#define CPPOPT_CAST_OPTVAL_HPP

#include <cstdint>
#include <exception>
#include <string>

#include "exception.hpp"

namespace cppopt {

/**
 * @brief Generic cast from string to a type.
 * @param param The parameter to convert.
 * @return The string converted to specified type `T`.
 *
 * Default implementation of type casting used if no explicit implementation is
 * provided.
 *
 * This depends on the existence of a constructor `T(const std::string&)` that
 * converts a string to a type. If that constructor does not provide the
 * desired casting behavior, this function must be explicitly implemented for
 * the type.
 *
 * @throws cppopt::invalid_cast If an exception was thrown by the
 *                              `T(const std::string&)` constructor. The error
 *                              message from that constructor will be used as
 *                              the error message for cppopt::invalid_cast
 *                              exception.
 */
template <typename T>
T cast_optval(const std::string& param)
{
    try {
        return T(param);
    } catch (const std::exception& e) {
        throw invalid_cast(e.what());
    }
}

template <> char cast_optval<char>(const std::string& param);

// These are provided instead of int, short, long, etc.
template <> std::int8_t cast_optval<std::int8_t>(const std::string& param);
template <> std::uint8_t cast_optval<std::uint8_t>(const std::string& param);
template <> std::int16_t cast_optval<std::int16_t>(const std::string& param);
template <> std::uint16_t cast_optval<std::uint16_t>(const std::string& param);
template <> std::int32_t cast_optval<std::int32_t>(const std::string& param);
template <> std::uint32_t cast_optval<std::uint32_t>(const std::string& param);
template <> std::int64_t cast_optval<std::int64_t>(const std::string& param);
template <> std::uint64_t cast_optval<std::uint64_t>(const std::string& param);

template <> float cast_optval<float>(const std::string& param);
template <> double cast_optval<double>(const std::string& param);
template <> long double cast_optval<long double>(const std::string& param);

} /* namespace cppopt */

#endif /* CPPOPT_CAST_OPTVAL_HPP */
