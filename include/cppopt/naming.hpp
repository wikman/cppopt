/**
 * @file   naming.hpp
 * @author John Wikman
 * @brief  Defines naming rules and utility function that provides
 *         functionality for each of the rules.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CPPOPT_NAMING_HPP
#define CPPOPT_NAMING_HPP

#include <string>

namespace cppopt {

/**
 * @brief Naming rules for option names.
 */
enum option_naming_rule {
    /**
     * The default naming rule. Allowed characters are:
     *  - ['a' ... 'z']
     *  - ['A' ... 'Z']
     *  - ['0' ... '9']
     *  - ['?', '!', '%', '+', '_', ':', ';', '|', '>', '<']
     *  - The character '-' is also allowed if a name does not start or end
     *    with that character.
     *  - The empty string is not allowed for long option names.
     */
    OPTNAME_DEFAULT,

    /**
     * Alphanumeric characters and hyphen only. Allowed characters are:
     *  - ['a' ... 'z']
     *  - ['A' ... 'Z']
     *  - ['0' ... '9']
     *  - The character '-' is also allowed if a name does not start or end
     *    with that character.
     *  - The empty string is not allowed for long option names.
     */
    OPTNAME_ALPHANUMERIC
};

const std::string& option_naming_rule_to_string(option_naming_rule) noexcept;

bool valid_short(char c, option_naming_rule rule = OPTNAME_DEFAULT) noexcept;
bool valid_long(const std::string& s, option_naming_rule rule = OPTNAME_DEFAULT) noexcept;

} /* namespace cppopt */

#endif /* CPPOPT_NAMING_HPP */
