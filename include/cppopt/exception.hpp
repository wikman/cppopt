/**
 * @file   exception.hpp
 * @author John Wikman
 * @brief  Defines the exceptions used internally within cppopt.
 *
 * These exceptions should never have to be checked by the user. All exceptions
 * that the user are exposed to through cppopt should be from the standard
 * library.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdexcept>
#include <string>

#ifndef CPPOPT_EXCEPTION_HPP
#define CPPOPT_EXCEPTION_HPP

namespace cppopt {

/**
 * @brief Indicates that a string could not be cast to a type.
 *
 * This is used to indicate whether something is wrong with the formatting of
 * an input value from the command line.
 *
 * @note This exception should only ever be checked for internally within
 *       cppopt and should not have to be checked manually in the user code.
 */
class invalid_cast: public std::invalid_argument {
public:
    /**
     * @brief Constructor.
     * @param what_arg The exception message.
     */
    invalid_cast(const std::string& what_arg): std::invalid_argument(what_arg) {}

    /**
     * @brief Constructor.
     * @param what_arg The exception message.
     */
    invalid_cast(const char* what_arg): std::invalid_argument(what_arg) {}
};

/**
 * @brief Indicates invalid usage of one of the classes.
 *
 * This is used to indicate that a cppopt class is not used as it should be.
 *
 * @note This exception should only ever be checked for internally within
 *       cppopt and should not have to be checked manually in the user code.
 */
class invalid_usage: public std::runtime_error {
public:
    /**
     * @brief Constructor.
     * @param what_arg The exception message.
     */
    invalid_usage(const std::string& what_arg): std::runtime_error(what_arg) {}

    /**
     * @brief Constructor.
     * @param what_arg The exception message.
     */
    invalid_usage(const char* what_arg): std::runtime_error(what_arg) {}
};

} /* namespace cppopt */

#endif /* CPPOPT_EXCEPTION_HPP */
