/**
 * @file   cppopt.h
 * @author John Wikman
 * @brief  Wrapper header to include all cppopt header files.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifdef __cplusplus

#include "cppopt/cast_optval.hpp"
#include "cppopt/exception.hpp"
#include "cppopt/flag.hpp"
#include "cppopt/help_printer.hpp"
#include "cppopt/incrementer.hpp"
#include "cppopt/modifier.hpp"
#include "cppopt/multi_param.hpp"
#include "cppopt/multi_positional.hpp"
#include "cppopt/naming.hpp"
#include "cppopt/option_base.hpp"
#include "cppopt/param.hpp"
#include "cppopt/parser.hpp"
#include "cppopt/positional.hpp"
#include "cppopt/positional_base.hpp"
#include "cppopt/version.hpp"

#endif /* __cplusplus */
