/**
 * @file   flag.cpp
 * @author John Wikman
 * @brief  Implements boolean flag option.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdexcept>
#include <string>
#include <vector>

#include <cppopt/flag.hpp>
#include <cppopt/modifier.hpp>
#include <cppopt/option_base.hpp>

namespace cppopt {

/**
 * @brief Constructor for a flag option with a short and long name.
 * @param shortopt The short "single-character" option name (i.e. `-S`).
 *                 Specifying the null character as the short option name will
 *                 result in no short option name being available for this
 *                 flag.
 * @param longopt Long option name. (i.e. `--longopt`)
 * @param description Description shown when printing the help message.
 * @param value_ref Pointer to the value to update on toggle.
 * @param mod Modifiers to be applied to this option. See cppopt::option_base
 *            for the effects that specific modifiers have.
 */
flag::flag(char shortopt, const std::string& longopt,
           const std::string& description,
           bool* value_ref,
           modifier mod):
    option_base(shortopt, longopt, {}, description, mod),
    m_value_ref(value_ref)
{
    // Do nothing here
}

/**
 * @brief Constructor for a flag option with only a long name.
 * @param longopt Long option name. (i.e. `--longopt`)
 * @param description Description shown when printing the help message.
 * @param value_ref Pointer to the value to update on toggle.
 * @param mod Modifiers to be applied to this option. See cppopt::option_base
 *            for the effects that specific modifiers have.
 */
flag::flag(const std::string& longopt,
           const std::string& description,
           bool* value_ref,
           modifier mod):
    flag('\0', longopt, description, value_ref, mod)
{
    // Do nothing here
}

/**
 * @brief Destructor. Default behavior.
 */
flag::~flag() {}

/**
 * @brief Copy-constructor.
 * @param other The flag object to copy from.
 */
flag::flag(const flag& other):
    option_base(other),
    m_value_ref(other.m_value_ref)
{
    // Do nothing here
}

/**
 * @brief Move-constructor.
 * @param other The flag object to move into this object.
 */
flag::flag(flag&& other) noexcept:
    option_base(other),
    m_value_ref(nullptr)
{
    std::swap(m_value_ref, other.m_value_ref);
}

/**
 * @brief Copy-assignment.
 * @param other The flag object to copy from.
 * @return A reference to this flag object after the copy.
 */
flag& flag::operator=(const flag& other)
{
    option_base::operator=(other);
    m_value_ref = other.m_value_ref;
    return *this;
}

/**
 * @brief Move-assignment.
 * @param other The flag object to move into this object.
 * @return A reference to this flag object after the move.
 */
flag& flag::operator=(flag&& other) noexcept
{
    option_base::operator=(other);
    std::swap(m_value_ref, other.m_value_ref);
    return *this;
}


/**
 * @brief Returns a dynamically allocated copy of the flag object.
 * @return An std::shared_ptr containing a dynamically allocated pointer to a
 *         cppopt::flag object.
 */
std::shared_ptr<option_base> flag::copy() const
{
    return std::shared_ptr<option_base>(new flag(*this));
}

/**
 * @brief The toggle function for a flag option.
 * @param params Parameters for this option.
 *
 * @note Internal cppopt usage only.
 */
void flag::m_internal_toggle(const std::vector<std::string>& params)
{
    (void) params;

    if (this->toggle_count() != 0) {
        // A flag toggle shall only take effect on the first toggle. No
        // exception thrown here since it should be OK that a flag is specified
        // multiple times.
        return;
    }

    // Invert the default boolean.
    *m_value_ref = !(*m_value_ref);
}

} /* namespace cppopt */
