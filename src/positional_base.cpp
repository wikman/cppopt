/**
 * @file   positional_base.cpp
 * @author John Wikman
 * @brief  Implements functionality for base class inherited by all
 *         positionals.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <memory>
#include <set>
#include <string>
#include <vector>

#include <cppopt/exception.hpp>
#include <cppopt/modifier.hpp>
#include <cppopt/positional_base.hpp>

namespace cppopt {
/**
 * @brief Constructor for the positional base class.
 * @param metavars The metavars for this positional. Shown when printing the
 *                 help message. The length of this vector determines how many
 *                 input arguments are expected for each toggle.
 * @param description Description shown when printing the help message.
 * @param mod Modifiers to apply to this positional. Currently supported
 *            modifiers are:
 *             - REQUIRED \n
 *               Makes the cppopt::positional_base::optional() function return
 *               false. Specify this modifier if this positional must be
 *               toggled on. This modifier is usually specified by default. Use
 *               cppopt::OPTIONAL to allow this positional argument to be
 *               omitted.
 * @param multiple_args Specifies whether or not the positional can be toggled
 *                      multiple time.
 *
 * @note This should only ever be invoked by subclasses.
 */
positional_base::positional_base(const std::vector<std::string>& metavars,
                                 const std::string& description,
                                 modifier mod,
                                 bool multiple_args):
    m_metavars(metavars),
    m_description(description),
    m_modifiers(mod),
    m_enum_values(metavars.size(), std::set<std::string>()),
    m_times_toggled(0),
    m_accepts_multiple(multiple_args)
{
    // Do nothing here
}

/**
 * @brief Destructor.
 */
positional_base::~positional_base() {}

/**
 * @brief Copy-constructor.
 * @param other The positional_base object to copy.
 */
positional_base::positional_base(const positional_base& other):
    m_metavars(other.m_metavars),
    m_description(other.m_description),
    m_modifiers(other.m_modifiers),
    m_enum_values(other.m_enum_values),
    m_times_toggled(other.m_times_toggled),
    m_accepts_multiple(other.m_accepts_multiple)
{
    // Do nothing here
}

/**
 * @brief Move-constructor.
 * @param other The positional_base object to move into this object.
 */
positional_base::positional_base(positional_base&& other) noexcept
{
    std::swap(m_metavars, other.m_metavars);
    std::swap(m_description, other.m_description);
    std::swap(m_modifiers, other.m_modifiers);
    std::swap(m_enum_values, other.m_enum_values);
    std::swap(m_times_toggled, other.m_times_toggled);
    std::swap(m_accepts_multiple, other.m_accepts_multiple);
}

/**
 * @brief Copy-assignment.
 * @param other The positional_base object to copy.
 * @return A reference to this object after the copy operation.
 */
positional_base& positional_base::operator=(const positional_base& other)
{
    m_metavars = other.m_metavars;
    m_description = other.m_description;
    m_modifiers = other.m_modifiers;
    m_enum_values = other.m_enum_values;
    m_times_toggled = other.m_times_toggled;
    m_accepts_multiple = other.m_accepts_multiple;
    return *this;
}

/**
 * @brief Move-assignment.
 * @param other The positional_base object to move into this object.
 * @return A reference to this object after the move operation.
 */
positional_base& positional_base::operator=(positional_base&& other) noexcept
{
    std::swap(m_metavars, other.m_metavars);
    std::swap(m_description, other.m_description);
    std::swap(m_modifiers, other.m_modifiers);
    std::swap(m_enum_values, other.m_enum_values);
    std::swap(m_times_toggled, other.m_times_toggled);
    std::swap(m_accepts_multiple, other.m_accepts_multiple);
    return *this;
}

/**
 * @brief Returns a dynamically allocated copy of the positional_base object.
 * @return An std::shared_ptr containing a dynamically allocated pointer to a
 *         cppopt::positional_base object.
 * @note This is virtual as to indicate that subclasses will copied
 *       differently.
 */
std::shared_ptr<positional_base> positional_base::copy() const
{
    return std::shared_ptr<positional_base>(new positional_base(*this));
}

/**
 * @brief Toggle this positional.
 * @param args The input arguments to toggle this positional on.
 *
 * @throws cppopt::invalid_usage If the number of arguments provided do not
 *                               match the number of configured meta variables
 *                               for this positional.
 * @throws cppopt::invalid_cast If one of the arguments does not match one of
 *                              its enumerated values (if configured), or if
 *                              something went wrong converting an argument to
 *                              a type.
 */
void positional_base::toggle(const std::vector<std::string>& args)
{
    if (this->arg_count() != args.size())
        throw cppopt::invalid_usage("number of provided arguments does not match the number of arguments it accepts");

    // Check for enumerated values
    for (std::size_t i = 0; i < args.size(); ++i) {
        if (m_enum_values[i].size() == 0) {
            // No enums for this parameter
            continue;
        }

        if (m_enum_values[i].find(args[i]) == m_enum_values[i].end())
            throw cppopt::invalid_cast("\"" + args[i] + "\" does not match any specified enum value");
    }

    m_internal_toggle(args);
    m_times_toggled += 1;
}

/**
 * @brief The internal toggle function. This should never be invoked directly
 *        on the cppopt::positional_base class.
 * @param args Input arguments for this positional.
 *
 * @throws cppopt::invalid_usage If invoked. This should only ever be invoked
 *                               on subclasses of positional_base.
 */
void positional_base::m_internal_toggle(const std::vector<std::string>& args)
{
    (void) args;

    throw cppopt::invalid_usage("toggle() invoked directly on the cppopt::positional_base class");
}

/**
 * @brief Sets enumerated values for each of the arguments.
 * @param enum_values Vector containing sets of enumerated values for each
 *                    positional argument in this object.
 *
 * @note The empty set indicates that any input value is valid.
 *
 * @warning Will overwrite any previously configured enumerated values.
 *
 * @throws std::invalid_argument If length of enum_values does not match the
 *                               number of configured arguments for this
 *                               positional.
 */
void positional_base::set_enums(const std::vector<std::set<std::string>>& enum_values)
{
    if (enum_values.size() != this->arg_count())
        throw cppopt::invalid_usage("length of enumeration vector does not match the number of positional arguments");

    m_enum_values = enum_values;
}

/**
 * @brief Returns the description for this positional.
 */
const std::string& positional_base::description() const
{
    return m_description;
}

/**
 * @brief Returns the metavars for this positional.
 */
const std::vector<std::string>& positional_base::metavars() const
{
    return m_metavars;
}

/**
 * @brief Returns the enumerated values for this positional.
 */
const std::vector<std::set<std::string>>& positional_base::enums() const
{
    return m_enum_values;
}

/**
 * @brief Returns the modifiers of this positional.
 */
modifier positional_base::modifiers() const
{
    return m_modifiers;
}

/**
 * @brief Returns true if this positional has been toggled, otherwise false.
 */
bool positional_base::toggled() const
{
    return m_times_toggled > 0;
}

/**
 * @brief Returns the number of times that this positional has been toggled.
 */
std::size_t positional_base::toggle_count() const
{
    return m_times_toggled;
}

/**
 * @brief Returns true if this positional can be toggled multiple times.
 *        Otherwise false.
 */
bool positional_base::multi_togglable() const
{
    return m_accepts_multiple;
}

/**
 * @brief Returns true if it is optional to provide values to this positional.
 *        Otherwise false.
 */
bool positional_base::optional() const
{
    return (m_modifiers & REQUIRED) != REQUIRED;
}

/**
 * @brief Returns the number of arguments that this positional accepts.
 */
std::size_t positional_base::arg_count() const
{
    return m_metavars.size();
}


} /* namespace cppopt */
