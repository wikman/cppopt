/**
 * @file   help_printer.cpp
 * @author John Wikman
 * @brief  Class for printing out help messages.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <ostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <cppopt/help_printer.hpp>

namespace cppopt {

/**
 * @brief Constructor for help_printer.
 * @param progname The initial program name to use. This can be change later by
 *        set_progname().
 *
 * This will set up a help_printer with no description, no positional
 * arguments, no optional arguments, no option termination token, and a fixed
 * maximum line width of 80 characters.
 */
help_printer::help_printer(const std::string& progname):
    m_progname(progname),
    m_description(""),
    m_positionals({}),
    m_options({}),
    m_optterm_token(""),
    m_has_optterm_token(false),
    m_use_fixed_maxwidth(true),
    m_fixed_maxwidth(80)
{
    // No special behavior here
}


/**
 * @brief Destructor. Default behavior.
 */
help_printer::~help_printer() {}


/**
 * @brief Copy-constructor.
 * @param other The help_printer object to copy from.
 */
help_printer::help_printer(const help_printer& other):
    m_progname(other.m_progname),
    m_description(other.m_description),
    m_positionals(other.m_positionals),
    m_options(other.m_options),
    m_optterm_token(other.m_optterm_token),
    m_has_optterm_token(other.m_has_optterm_token),
    m_use_fixed_maxwidth(other.m_use_fixed_maxwidth),
    m_fixed_maxwidth(other.m_fixed_maxwidth)
{
    // No special behavior here
}


/**
 * @brief Move-constructor.
 * @param other The help_printer object to move into this object.
 */
help_printer::help_printer(help_printer&& other) noexcept: help_printer()
{
    this->operator=(other);
}


/**
 * @brief Copy-assignment.
 * @param other The help_printer object to copy from.
 */
help_printer& help_printer::operator=(const help_printer& other)
{
    *this = help_printer(other);
    return *this;
}


/**
 * @brief Move-assignment.
 * @param other The help_printer object to move into this object.
 */
help_printer& help_printer::operator=(help_printer&& other) noexcept
{
    std::swap(m_progname, other.m_progname);
    std::swap(m_description, other.m_description);
    std::swap(m_positionals, other.m_positionals);
    std::swap(m_options, other.m_options);
    std::swap(m_optterm_token, other.m_optterm_token);
    std::swap(m_has_optterm_token, other.m_has_optterm_token);
    std::swap(m_use_fixed_maxwidth, other.m_use_fixed_maxwidth);
    std::swap(m_fixed_maxwidth, other.m_fixed_maxwidth);

    return *this;
}


/**
 * @brief Sets the program name to be displayed in help prints.
 * @param progname The program name to be used.
 */
void help_printer::set_progname(const std::string& progname)
{
    m_progname = progname;
}


/**
 * @brief Sets the description to be displayed in help prints.
 * @param description The description to be used. Overwrites any previously
 *                    set description.
 * @note The help printer will interpret the empty string as the description
 *       being non-existent and skip printing it.
 */
void help_printer::set_description(const std::string& description)
{
    m_description = description;
}


/**
 * @brief Sets the option termination token to be displayed in the help prints.
 * @param token The option termination token.
 * @note As well as setting the option termination token, this also tells the
 *       help_printer that an option termination token shall be used.
 */
void help_printer::set_optterm_token(const std::string& token)
{
    m_optterm_token = token;
    m_has_optterm_token = true;
}


/**
 * @brief Configures the help printer to print with this specific maximum line
 *        width.
 * @param maxwidth The fixed maximum line width to use.
 * @note This also sets the maximum line width mode to fixed.
 */
void help_printer::set_fixed_maxwidth(std::size_t maxwidth)
{
    m_use_fixed_maxwidth = true;
    m_fixed_maxwidth = maxwidth;
}


/**
 * @brief Sets the optional arguments to use for this help printer.
 * @param optvec A vector of std::shared_ptr to cppopt::option_base objects.
 *               These pointers shall be created by invoking the copy()
 *               function on the options.
 * @warning This will discard any previously added options.
 */
void help_printer::set_options(const std::vector<std::shared_ptr<option_base>>& optvec)
{
    m_options = optvec;
}


/**
 * @brief Sets the positional arguments to use for this help printer.
 * @param posvec A vector of std::shared_ptr to cppopt::positional_base
 *               objects. These pointers shall be created by invoking the
 *               copy() function on the options.
 * @warning This will discard any previously added positionals.
 */
void help_printer::set_positionals(const std::vector<std::shared_ptr<positional_base>>& posvec)
{
    m_positionals = posvec;
}


/**
 * @brief Prints a full help message with all sections.
 * @param os The output stream to print to.
 * @param interleaved_lines How many blank lines to print out between each
 *                          section/part.
 * @note This will always print out the usage. The remaining parts will only be
 *       printed if content exists for them and/or if they are enabled.
 */
void help_printer::print_full(std::ostream& os, std::size_t interleaved_lines) const
{
    print_usage(os);

    if (m_description.length() > 0) {
        for (std::size_t i = 0; i < interleaved_lines; ++i)
            os << std::endl;

        print_description(os);
    }

    if (m_positionals.size() > 0) {
        for (std::size_t i = 0; i < interleaved_lines; ++i)
            os << std::endl;

        print_positional_details(os);
    }

    if (m_options.size() > 0) {
        for (std::size_t i = 0; i < interleaved_lines; ++i)
            os << std::endl;

        print_optional_details(os);
    }
}


/**
 * @brief Prints an arbitrary string in accordance with the help_printer
 *        configuration.
 * @param os The output stream to print to.
 * @param s The string to print.
 */
void help_printer::print_string(std::ostream& os, const std::string& s) const
{
    print_string_indent_subseq_lines(os,
        {s},
        {true},
        0,
        true // fill linewidth
    );
}


/**
 * @brief Prints a brief usage message.
 * @param os The output stream to print to.
 */
void help_printer::print_usage(std::ostream& os) const
{
    std::size_t maxwidth = this->get_maxwidth();

    std::vector<std::string> usage_words = {"usage: ", m_progname};
    std::vector<bool> usage_formatflags = {false, false};

    std::size_t indent = 6 + 1 + m_progname.length() + 1;
    if (indent > (maxwidth / 2)) {
        // do not allow indentation that covers more than half of maxwidth
        indent = usage_words[0].length();
    }

    // Print option summaries in usage
    for (const std::shared_ptr<option_base> &opt : m_options) {
        std::ostringstream buf;
        buf << "[";
        if (opt->has_short())
            buf << '-' << opt->shortname();
        else
            buf << "--" << opt->longname();
        for (const std::string &mv : opt->metavars())
            buf << ' ' << mv;
        buf << ']';

        usage_words.push_back(" ");
        usage_formatflags.push_back(true);
        usage_words.push_back(buf.str());
        usage_formatflags.push_back(false);
    }

    // Print option termination token if present
    if (m_has_optterm_token) {
        std::ostringstream buf;
        buf << '[' << m_optterm_token << ']';

        usage_words.push_back(" ");
        usage_formatflags.push_back(true);
        usage_words.push_back(buf.str());
        usage_formatflags.push_back(false);
    }

    // Finally print positional summaries in usage
    for (const std::shared_ptr<positional_base> &pos : m_positionals) {
        std::ostringstream buf;
        if (pos->optional())
            buf << '[';
        else if (pos->multi_togglable() && (pos->arg_count() > 1))
            buf << '<';

        for (size_t i = 0; i < pos->arg_count(); ++i) {
            if (i > 0)
                buf << ' ';
            buf << pos->metavars()[i];
        }

        if (pos->optional())
            buf << ']';
        else if (pos->multi_togglable() && (pos->arg_count() > 1))
            buf << '>';

        if (pos->multi_togglable())
            buf << "...";

        usage_words.push_back(" ");
        usage_formatflags.push_back(true);
        usage_words.push_back(buf.str());
        usage_formatflags.push_back(false);
    }

    print_string_indent_subseq_lines(os,
        usage_words,
        usage_formatflags,
        indent,
        false // do not fill linewidth
    );
}


/**
 * @brief Prints out the description.
 * @param os The output stream to print to.
 */
void help_printer::print_description(std::ostream& os) const
{
    print_string_indent_subseq_lines(os,
        {m_description},
        {true},
        0,
        true // fill linewidth
    );
}


/**
 * @brief Prints out details about the positional arguments.
 * @param os The output stream to print to.
 */
void help_printer::print_positional_details(std::ostream& os) const
{
    print_string_indent_subseq_lines(os,
        {"Positional arguments:"},
        {true},
        0,   // no indent
        true // fill linewidth
    );
    for (const std::shared_ptr<positional_base> &pos : m_positionals) {
        std::ostringstream buf;
        buf << ' ';
        for (const std::string &mv : pos->metavars())
            buf << ' ' << mv;
        buf << " (";
        if (pos->optional())
            buf << "optional";
        else
            buf << "required";
        if (pos->multi_togglable())
            buf << ", accepts multiple";
        buf << ")";
        print_string_indent_subseq_lines(os,
            {buf.str(), "\n", pos->description()},
            {false,     true, true},
            4,
            true
        );
    }
}


/**
 * @brief Prints out details about the optional arguments.
 * @param os The output stream to print to.
 */
void help_printer::print_optional_details(std::ostream& os) const
{
    std::size_t maxwidth = this->get_maxwidth();

    // Find the largest cutoff point that is less than half of the line width
    std::size_t cutoff = 0;
    if (m_has_optterm_token) {
        // "  <opttermtoken> "
        std::size_t prelen = 2 + m_optterm_token.length() + 1;
        if (prelen < (maxwidth / 2))
            cutoff = std::max(cutoff, prelen);
    }
    for (const std::shared_ptr<option_base> &opt : m_options) {
        // "  [-S, ]--LONG [METAVARS...] "
        std::size_t prelen = 2 + 2 + opt->longname().length() + 1;
        if (opt->has_short())
            prelen += 4;
        for (const std::string& mv : opt->metavars())
            prelen += 1 + mv.length();
        if (prelen < (maxwidth / 2))
            cutoff = std::max(cutoff, prelen);
    }

    // Print out the descriptions
    print_string_indent_subseq_lines(os,
        {"Optional arguments:"},
        {true},
        0,   // no indent
        true // fill linewidth
    );
    if (m_has_optterm_token) {
        if (cutoff >= 4) {
            std::size_t padding = cutoff - 4;
            print_string_indent_subseq_lines(os,
                {"  ",  m_optterm_token, std::string(padding, ' '), "Terminates option parsing."},
                {false, false,           false,                     true},
                cutoff,
                true // fill linewidth
            );
        } else {
            print_string_indent_subseq_lines(os,
                {"  ",  m_optterm_token, "\n",  "Terminates option parsing."},
                {false, false,           true, true},
                4,
                true // fill linewidth
            );
        }
    }
    for (const std::shared_ptr<option_base> &opt : m_options) {
        std::ostringstream preamble;
        preamble << "  ";
        if (opt->has_short())
            preamble << '-' << opt->shortname() << ", ";
        preamble << "--" << opt->longname();
        for (const std::string& mv : opt->metavars())
            preamble << ' ' << mv;

        std::string reqstr = "";
        if (opt->required())
            reqstr = " (required)";

        // Account for extra space, not adding it directly to stream since it
        // is not used in the second case
        std::size_t prelen = ((std::size_t) preamble.tellp()) + 1;
        if (cutoff >= prelen) {
            preamble << ' ';
            std::size_t padding = cutoff - prelen;
            print_string_indent_subseq_lines(os,
                {preamble.str(), std::string(padding, ' '), opt->description(), reqstr},
                {false,          false,                     true,               true},
                cutoff,
                true // fill linewidth
            );
        } else {
            print_string_indent_subseq_lines(os,
                {preamble.str(), "\n", opt->description(), reqstr},
                {false,          true, true,               true},
                4,
                true // fill linewidth
            );
        }
    }
}


/**
 * @brief Get the maximum line width of the help printer.
 * @return The maximum line width.
 *
 * @todo Implement different modes. Currently only returns a fixed maxwidth.
 */
std::size_t help_printer::get_maxwidth() const
{
    return m_fixed_maxwidth;
}


/**
 * @brief Prints out the provided strings, indents any subsequent lines by the
 *        specified amount if a line break occurs.
 * @param os The output stream to print to.
 * @param strs The strings to print. These strings will be printed in order as
 *             if they were concatenated.
 * @param enable_reformatting Flags for each string in `strs`. If one of these
 *                            is set to false for a string, then every
 *                            character will be printed as is without any
 *                            special behavior for certain characters, treating
 *                            all characters in that string as if they are part
 *                            of a single word. If set to true, certain
 *                            characters will get special behavior, such as a
 *                            space representing a separator between words, a
 *                            '\n' character will line break and indent, etc.
 * @param indent The indentation level to use on line breaks.
 * @param fill_linewidth Whether or not to add additional spaces such to fill a
 *                       line to its maximum width if an overflow triggered a
 *                       line break.
 *
 * @note This does not insert any space between strings. It will print the
 *       input {"Get", "me", "water."} as "Getmewater.".
 * @note `strs` and `enable_reformatting` must be of same size. Otherwise
 *       std::runtime_error will be thrown.
 *
 * @todo UTF-8 support.
 */
void help_printer::print_string_indent_subseq_lines(std::ostream& os,
                                                    const std::vector<std::string>& strs,
                                                    const std::vector<bool>& enable_reformatting,
                                                    std::size_t indent,
                                                    bool fill_linewidth) const
{
    std::size_t maxwidth = this->get_maxwidth();

    // Allow us to further assume that strs.size() > 0
    if (strs.size() == 0) {
        os << std::endl;
        return;
    }

    // TODO: Maybe this should be pairs instead of two separate vectors?
    if (strs.size() != enable_reformatting.size())
        throw std::runtime_error("size mismatch for string size and reformatting flags");

    std::vector<std::string> linewords;
    std::size_t linelength = 0;
    std::ostringstream curword;
    std::size_t start_stridx = 0;
    std::size_t start_chidx = 0;
    // Find start of first word
    // (If reformatting is enabled, leading spaces, tabs, and line breaks will
    // be discarded.)
    bool found_fstword = false;
    for (std::size_t stridx = 0; stridx < strs.size(); ++stridx) {
        if (!enable_reformatting[stridx]) {
            start_stridx = stridx;
            start_chidx = 0;
            found_fstword = true;
            break;
        }
        const std::string& s = strs[stridx];
        for (std::size_t chidx = 0; chidx < s.length(); ++chidx) {
            if (!std::isspace((unsigned char) s[chidx])) {
                start_stridx = stridx;
                start_chidx = chidx;
                found_fstword = true;
                break;
            }
        }
        if (found_fstword)
            break;
    }

    if (!found_fstword) {
        // No words to print, output line break and return
        os << std::endl;
        return;
    }

    std::size_t preindent_by = 0;
    bool scanning_word = false;
    for (std::size_t stridx = start_stridx; stridx < strs.size(); ++stridx) {
        const std::string& s = strs[stridx];

        std::size_t init_chidx = 0;
        if (stridx == start_stridx)
            init_chidx = start_chidx;

        for (std::size_t chidx = init_chidx; chidx < s.length(); ++chidx) {
            char c = s[chidx];
            bool is_wordchar = false;
            if (!enable_reformatting[stridx] || !std::isspace((unsigned char) c))
                is_wordchar = true;

            if (is_wordchar) {
                curword.put(c);
                scanning_word = true;
            } else {
                bool printed_line = false;
                if (scanning_word) {
                    // Commit new word to vector
                    linelength += curword.tellp();
                    if (linewords.size() > 0)
                        linelength += 1; // add interleaving space

                    if (linelength > maxwidth) {
                        // Exceeded linelimit, flush current line before adding new word
                        indent_and_print_line(os,
                            linewords,
                            preindent_by,
                            fill_linewidth
                        );
                        linewords.clear();
                        linelength = indent + curword.tellp();
                        printed_line = true;
                    }
                    // Add new word to line
                    linewords.push_back(curword.str());
                    curword.str(""); // reset word buf
                    scanning_word = false;
                }

                if (c == '\n' && !printed_line) {
                    // forced linebreak
                    indent_and_print_line(os,
                        linewords,
                        preindent_by,
                        false // do not fill linewidth on forced linebreak
                    );
                    linewords.clear();
                    linelength = indent;
                    printed_line = true;
                }

                if (printed_line) {
                    // Set indentation for next line
                    preindent_by = indent;
                }
            }
        }
    }

    // Check if there is a trailing word
    // TODO: Merge this in with the normal case
    if (curword.tellp() > 0) {
        // Commit new word to vector
        linelength += curword.tellp();
        if (linewords.size() > 0)
            linelength += 1; // add interleaving space

        if (linelength > maxwidth) {
            // Exceeded linelimit, flush current line before adding new word
            indent_and_print_line(os,
                linewords,
                preindent_by,
                fill_linewidth
            );
            linewords.clear();
            linelength = indent + curword.tellp();
            preindent_by = indent;
        }
        // Add new word to line
        linewords.push_back(curword.str());
        curword.str(""); // reset word buf
    }

    // Print out any remaining words
    if (linewords.size() > 0) {
        indent_and_print_line(os,
            linewords,
            preindent_by,
            false // do not fill linewidth
        );
    }

    return;
}


/**
 * @brief Indents and prints vector of words. Helper function to
 *        print_string_indent_subseq_lines.
 * @param os The output stream to print to.
 * @param words The words to print.
 * @param indent How many spaces should be prefixed on the line.
 * @param fill_linewidth Whether or not to interleave additional spaces between
 *                       the words such that the line length is equal to the
 *                       maximum width.
 *
 * @note Spaces will be inserted between the entered words. No spaces should
 *       have to be provided manually.
 */
void help_printer::indent_and_print_line(std::ostream& os,
                                         const std::vector<std::string>& words,
                                         std::size_t indent,
                                         bool fill_linewidth) const
{
    std::size_t maxwidth = this->get_maxwidth();
    if (maxwidth < indent)
        maxwidth = 1;
    else
        maxwidth = maxwidth - indent;

    // Base case with no words or 1 word, eliminate these possibilities early
    if (words.size() == 0) {
        os << std::endl;
        return;
    }
    if (words.size() == 1) {
        os << std::string(indent, ' ') << words[0] << std::endl;
        return;
    }

    // Now we know that at least 2 words exists
    std::size_t wordchars = 0;
    for (const std::string& s : words)
        wordchars += s.length();

    std::size_t rem_spacing = words.size() - 1;
    if (fill_linewidth && wordchars < maxwidth)
        rem_spacing = std::max(rem_spacing, maxwidth - wordchars);

    // How many long spaces we have to insert
    std::lldiv_t f = std::lldiv((long long) rem_spacing, (long long) (words.size() - 1));
    std::size_t spacing = f.quot;

    std::size_t irregularities = (std::size_t) f.rem;
    std::size_t irregularity_increments = maxwidth / (irregularities + 1);

    // indent and print out first word separately since it does not have an
    // interleaving space prior to it
    os << std::string(indent, ' ') << words[0];

    std::size_t printed_chars = words[0].length();
    std::size_t next_irregularity_threshold = irregularity_increments;
    for (std::size_t i = 1; i < words.size(); ++i) {
        std::size_t spaces = spacing;

        // Balancing of where to place additional spacing
        if (irregularities > 0) {
            if (printed_chars >= next_irregularity_threshold) {
                spaces += 1;
                irregularities -= 1;
                next_irregularity_threshold += irregularity_increments;
            }
        }

        os << std::string(spaces, ' ') << words[i];
        printed_chars += spaces + words[i].length();
    }

    os << std::endl;
}


} /* namespace cppopt */
