/**
 * @file   parser.cpp
 * @author John Wikman
 * @brief  Implements the parser class.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cctype>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>


#include <cppopt/exception.hpp>
#include <cppopt/flag.hpp>
#include <cppopt/help_printer.hpp>
#include <cppopt/naming.hpp>
#include <cppopt/parser.hpp>

namespace cppopt {


/**
 * @brief Constructor.
 * @param description Program description shown when printing the help message.
 * @param help_shortopt The short name for the built-in help option. Specify
 *                      '\0' to have no short name for the help option.
 * @param help_longopt The long name for the built-in help option.
 *
 * The built-in help option will always be present and will print the full help
 * message when toggled. Depending on the configuration of
 * parser::exit_on_error, triggering the help message will either exit the
 * program with code 0 or throw an std::invalid_argument exception.
 *
 * @note Sets a default maximum line width to 80 characters when printing the
 *       help message.
 * @note Will default to exit the program if an error is encountered when
 *       parsing the input arguments. Set parser::exit_on_error to false to
 *       instead trigger an std::invalid_argument exception that can be caught
 *       by the user code.
 * @note Option naming rule is set to cppopt::OPTNAME_DEFAULT. If a different
 *       rule is desired, this should be changed before adding any optional
 *       arguments.
 * @note No option termination token is configured by default. Configure an
 *       option termination token through set_optterm_token() if this is
 *       desired.
 *
 * @see exit_on_error
 * @see set_optterm_token
 */
parser::parser(const std::string& description,
               char help_shortopt,
               const std::string& help_longopt):
    m_description(description),
    m_optterm_token(""),
    m_optterm_enabled(false),
    m_opt_rule(OPTNAME_DEFAULT),
    m_exit_on_error(true)
{
    m_help_triggered = false;
    m_help_longname = help_longopt;
    this->add(flag(
        help_shortopt,
        help_longopt,
        "Displays a help message and exits.",
        &m_help_triggered
    ));

    m_help_printer.set_fixed_maxwidth(80);
}


/**
 * @brief Destructor.
 */
parser::~parser() {}


/**
 * @brief Copy-constructor.
 * @param other The parser object to copy from.
 */
parser::parser(const parser& other):
    m_description(other.m_description),
    m_positionals(other.m_positionals),
    m_options(other.m_options),
    m_shortmap(other.m_shortmap),
    m_longmap(other.m_longmap),
    m_optterm_token(other.m_optterm_token),
    m_optterm_enabled(other.m_optterm_enabled),
    m_opt_rule(other.m_opt_rule),
    m_help_triggered(other.m_help_triggered),
    m_help_longname(other.m_help_longname),
    m_exit_on_error(other.m_exit_on_error),
    m_help_printer(other.m_help_printer)
{
    // We need to re-add the help option to fix the pointer of the help flag.
    int idx = m_longmap[m_help_longname];
    std::shared_ptr<option_base> prev_help = m_options[idx];
    m_options[idx] = flag(
        prev_help->shortname(),
        prev_help->longname(),
        prev_help->description(),
        &m_help_triggered
    ).copy();

    // Also need to re-copy all shared_ptr objects such that the parsing in one
    // parser does not affect another.
    for (std::size_t i = 0; i < m_options.size(); ++i)
        m_options[i] = m_options[i]->copy();
    for (std::size_t i = 0; i < m_positionals.size(); ++i)
        m_positionals[i] = m_positionals[i]->copy();
}


/**
 * @brief Move-constructor.
 * @param other The parser object to move into this object.
 */
parser::parser(parser&& other) noexcept: parser()
{
    // Important to invoke the default constructor before this such that
    // crucial state variables are set up
    this->operator=(other);
}


/**
 * @brief Copy-assignment.
 * @param other The parser object to copy from.
 * @return A reference to this object after the copy operation.
 */
parser& parser::operator=(const parser& other)
{
    *this = parser(other);
    return *this;
}


/**
 * @brief Move-assignment.
 * @param other The parser object to move into this object.
 * @return A reference to this object after the move operation.
 */
parser& parser::operator=(parser&& other) noexcept
{
    std::swap(m_description, other.m_description);
    std::swap(m_positionals, other.m_positionals);
    std::swap(m_options, other.m_options);
    std::swap(m_shortmap, other.m_shortmap);
    std::swap(m_longmap, other.m_longmap);
    std::swap(m_optterm_token, other.m_optterm_token);
    std::swap(m_optterm_enabled, other.m_optterm_enabled);
    std::swap(m_opt_rule, other.m_opt_rule);
    std::swap(m_help_triggered, other.m_help_triggered);
    std::swap(m_help_longname, other.m_help_longname);
    std::swap(m_exit_on_error, other.m_exit_on_error);
    std::swap(m_help_printer, other.m_help_printer);

    // We need to re-add the help option to fix the pointer of the help flag.
    int idx = m_longmap[m_help_longname];
    std::shared_ptr<option_base> prev_help = m_options[idx];
    m_options[idx] = flag(
        prev_help->shortname(),
        prev_help->longname(),
        prev_help->description(),
        &m_help_triggered
    ).copy();

    // Also need to fix the help value for the passed rvalue
    int other_idx = other.m_longmap[other.m_help_longname];
    std::shared_ptr<option_base> other_help = other.m_options[other_idx];
    other.m_options[other_idx] = flag(
        other_help->shortname(),
        other_help->longname(),
        other_help->description(),
        &other.m_help_triggered
    ).copy();

    return *this;
}


/**
 * @brief Parse input arguments using argument format from the main function.
 * @param argc Number of arguments in argv.
 * @param argv Pointer to a sequence of C-style strings that contain the input
 *             arguments.
 *
 * @note Convenience function such as not having to cast argv to const. The
 *       contents of argv will not be modified by this function.
 * @note The first input argument in argv will denote the program name.
 *
 * @throws std::invalid_argument If argc is negative, if argv is NULL, or if
 *                               something else goes wrong during parsing and
 *                               the parser is configured to not exit on error.
 *
 * @see parse(const std::vector<std::string>& argv)
 */
void parser::parse(int argc, char** argv)
{
    this->parse(argc, (const char**) argv);
}


/**
 * @brief Parse input arguments using argument format from the main function.
 * @param argc Number of arguments in argv.
 * @param argv Pointer to a sequence of C-style strings that contain the input
 *             arguments.
 *
 * @note The first input argument in argv will denote the program name.
 *
 * @throws std::invalid_argument If argc is negative, if argv is NULL, or if
 *                               something else goes wrong during parsing and
 *                               the parser is configured to not exit on error.
 *
 * @see parse(const std::vector<std::string>& argv)
 */
void parser::parse(int argc, const char** argv)
{
    if (argc < 0)
        throw std::invalid_argument("Negative argc value");
    if (argv == nullptr)
        throw std::invalid_argument("argv cannot be NULL");

    // Convert input to a vector of strings, then parse. Using the pointer
    // values as iterator begin and iterator end to initialize the vector.
    this->parse(std::vector<std::string>(&argv[0], &argv[argc]));
}


/**
 * @brief Parse input arguments in terms of a vector of strings.
 * @param argv Vector of strings that contains the input arguments.
 *
 * Input arguments from argv are parsed in order where the first argument will
 * be interpreted as the program name. If an input argument starts with a
 * hyphen (`-`), it will be interpreted as optional argument unless the option
 * termination token has been provided in argv. The option termination token
 * always has prescedence and will be the first thing that the input argument
 * is checked against. After the option termination token has been matched on,
 * it will not be checked for again. Such that if a positional argument with
 * the contents of the option termination token is necessary, the option
 * termination token has to be specified twice (once to terminate option
 * parsing and then for the positional argument). See set_optterm_token() for
 * more details.
 *
 * When interpreting an input argument as an option, it will first try to
 * interpret the input argument as a long option name. A long option name is
 * denoted by a double hyphen (`--`) followed by the longopt name specified in
 * the optional argument. The parsing rules for long option names (using the
 * name "my-opt" as an example) are:
 *  - `{"--my-opt"}` \n
 *    (no parameters)
 *  - `{"--my-opt", "PARAM"}` \n
 *    (one parameter)
 *  - `{"--my-opt=PARAM"}` \n
 *    (one parameter, separated by equals sign)
 *  - `{"--my-opt", "PARAM_1", "PARAM_2", ..., "PARAM_n"}` \n
 *    (multiple parameters)
 *  - `{"--my-opt=PARAM_1", "PARAM_2", ..., "PARAM_n"}` \n
 *    (multiple parameters, first separated by equals sign)
 *
 * If the option does not start with a double hyphen (`--`), it will be
 * attempted to be parsed as a short option. Using short options `v`, `x`, `T`,
 * and `f` as examples, the parsing rules for short option names are:
 *  - `{"-v"}` \n
 *    (single short option, no parameters)
 *  - `{"-v", "-x"}` \n
 *    (two short options, no parameters)
 *  - `{"-vx"}` \n
 *    (two compund short options, no parameters, same effect as previous item)
 *  - `{"-vxT"}` \n
 *    (three compund short options, no parameters)
 *  - `{"-f", "VALUE"}` \n
 *    (single short option, one parameter)
 *  - `{"-f=VALUE"}` \n
 *    (single short option, one parameter separated by equals sign, same effect
 *    as previous item)
 *  - `{"-fVALUE"}` \n
 *    (single short option, one compund parameter, same effect as previous two
 *    items)
 *  - `{"-vxTf", "VALUE"}` \n
 *    (four compund short options, one parameter)
 *  - `{"-vxTf=VALUE"}` \n
 *    (four compund short options, one parameter separated by equals sign, same
 *    effect as previous item)
 *  - `{"-vxTfVALUE"}` \n
 *    (four compund short options, one compund parameter, same effect as
 *    previous two items)
 *  - `{"-vxf=VALUE", "-T"}` \n
 *    (four short options, one parameter)
 *  - `{"-f", "VALUE_1", "VALUE_2", ..., "VALUE_n"}` \n
 *    (single short option, multiple parameters)
 *  - `{"-f=VALUE_1", "VALUE_2", ..., "VALUE_n"}` \n
 *    (single short option, multiple parameters, first parameter separated by
 *    equals sign, same effect as previous item)
 *  - `{"-fVALUE_1", "VALUE_2", ..., "VALUE_n"}` \n
 *    (single short option, multiple parameters, first parameter compund, same
 *    effect as previous two items)
 *  - `{"-vfVALUE_1", "VALUE_2", ..., "VALUE_n"}` \n
 *    (two compund short options, multiple parameters, first parameter compund)
 *
 * Positional arguments have no special syntax and will be parsed as they
 * appear as input arguments. If it is necessary to have positional arguments
 * starting with a hyphen, then the option termination token must be set
 * through set_optterm_token() and the option termination token provided as
 * an input argument before the positional argument starting with a hyphen.
 *
 * @note The first input argument in argv will denote the program name.
 *
 * @throws std::invalid_argument If an error occurs during parsing and the
 *                               parser is configured to not exit on error.
 *
 * @see exit_on_error
 * @see set_optterm_token
 */
void parser::parse(const std::vector<std::string>& argv)
{
    if (argv.size() < 1)
        this->error("Must have at least one argument to parse (the program name argument).");

    const std::string& progname = argv[0];
    std::size_t posidx = 0;
    bool optparsing_enabled = true;

    m_help_printer.set_progname(progname);
    m_help_printer.set_description(m_description);
    m_help_printer.set_positionals(m_positionals);
    m_help_printer.set_options(m_options);
    if (m_optterm_enabled)
        m_help_printer.set_optterm_token(m_optterm_token);

    for (std::size_t i = 1; i < argv.size(); ++i) {
        std::size_t parsed_option_args = 0;

        // Try to parse option args first
        if (optparsing_enabled) {
            if (m_optterm_enabled && (argv[i] == m_optterm_token)) {
                // Reached option termination token, no more parsing of options
                optparsing_enabled = false;
                continue;
            }

            parsed_option_args = this->parse_option(argv, i);
            if (parsed_option_args > 0) {
                // Found option, shift counter along and continue
                // (-1 to account for auto increment in for-loop)
                i += (parsed_option_args - 1);

                // Just check before if the help message was triggered. In which
                // case it should be displayed on stdout.
                if (m_help_triggered) {
                    m_help_printer.print_full(std::cerr);
                    if (m_exit_on_error) {
                        std::exit(0);
                    } else {
                        // TODO: A better exception to use here maybe?
                        this->error("help triggered");
                    }
                }

                continue;
            }
        }

        // Parse positional

        // Check that there is a positional that we can parse
        if (posidx >= m_positionals.size()) {
            std::ostringstream msg;
            msg << "Unexpected positional \"" << argv[i] << "\".";
            this->error(msg.str());
        }

        // Validate argument count
        const std::shared_ptr<positional_base>& pos = m_positionals[posidx];
        std::size_t provided_args = argv.size() - i;
        if (provided_args < pos->arg_count()) {
            std::ostringstream msg;
            msg << "Too few positional arguments remaining for";
            for (const std::string& mv : pos->metavars())
                msg << " " << mv;
            msg << ". " << provided_args << " provided. " << pos->arg_count()
                << " required.";
            this->error(msg.str());
        }

        std::vector<std::string> posargs;
        for (std::size_t j = 0; j < pos->arg_count(); ++j)
            posargs.push_back(argv[i + j]);

        try {
            pos->toggle(posargs);
        } catch (const invalid_usage &e) {
            std::ostringstream msg;
            msg << "Error parsing positional";
            for (const std::string& mv : pos->metavars())
                msg << " " << mv;
            msg << ": " << e.what();
            this->error(msg.str());
        } catch (const invalid_cast& e) {
            std::ostringstream msg;
            msg << "Error casting argument for positional";
            for (const std::string& mv : pos->metavars())
                msg << " " << mv;
            msg << ": " << e.what();
            this->error(msg.str());
        }

        // If this is not multi-togglable, then shift it along
        if (!pos->multi_togglable())
            posidx += 1;

        // Shift by the number of parser arguments
        // (-1 to account for auto increment in for-loop)
        i += (pos->arg_count() - 1);
    }

    // Check that we are not missing a positional (non-optional argument remains that have not been toggled)
    if ((posidx < m_positionals.size()) && !m_positionals[posidx]->optional() && !m_positionals[posidx]->toggled()) {
        std::ostringstream msg;
        msg << "Missing positional arguments for";
        for (const std::string &mv : m_positionals[posidx]->metavars())
            msg << " " << mv;
        msg << ".";
        this->error(msg.str());
    }

    // Check that we are not missing a required option
    for (const std::shared_ptr<option_base>& opt : m_options) {
        if (opt->required() && !opt->toggled()) {
            std::ostringstream msg;
            msg << "Missing required optional argument --" << opt->longname()
                << ".";
            this->error(msg.str());
        }
    }
}


/**
 * @brief Produces an error condition for the argument parsing.
 * @param msg The error message to be shown.
 *
 * If parser is configured to exit on error, this will print the msg string
 * together with a usage description and the exit with code 1. Otherwise an
 * std::invalid_argument exception will be thrown that contains only the
 * entered message.
 *
 * If exit on error is configured, then the msg string will be formatted in
 * accordance with the help printing configuration.
 *
 * @note If invoking this function from user code, it should only be invoked
 *       after an invocation to parse(). Otherwise crucial state variables
 *       might not have been set up properly.
 *
 * @throws std::invalid_argument If parser is configured to not exit on error.
 *
 * @see exit_on_error
 */
void parser::error(const std::string& msg)
{
    if (m_exit_on_error) {
        m_help_printer.set_positionals(m_positionals);
        m_help_printer.set_options(m_options);

        m_help_printer.print_string(std::cerr, msg);
        std::cerr << std::endl;

        std::ostringstream buf;
        buf << "Run with --" << m_help_longname
            << " for a full usage description.";
        m_help_printer.print_string(std::cerr, buf.str());
        std::cerr << std::endl;

        m_help_printer.print_usage(std::cerr);

        std::exit(1);
    } else {
        throw std::invalid_argument(msg);
    }
}


/**
 * @brief Sets the option termination token and enables it during parsing.
 * @param token The option termination token to use.
 *
 * There is no limit on the value that the option termination token can take
 * on. It will be checked with string equality against input arguments.
 *
 * @see parse
 */
void parser::set_optterm_token(const std::string& token)
{
    m_optterm_token = token;
    m_optterm_enabled = true;
}


/**
 * @brief Sets the naming rule for options.
 * @param rule The option name rule to use.
 * @param error_on_present_invalids Whether or not to check if all previously
 *                                  added options also comply with the entered
 *                                  option naming rule.
 *
 * @throws std::invalid_argument If error_on_present_invalids is set to true
 *                               and that at least one of the previously
 *                               entered options do not comply with the
 *                               provided naming rule.
 */
void parser::set_option_naming_rule(option_naming_rule rule,
                                    bool error_on_present_invalids)
{
    if (error_on_present_invalids) {
        std::ostringstream msg;
        msg << "Option naming rule " << option_naming_rule_to_string(rule)
            << " invalid for existing ";
        for (const std::shared_ptr<option_base>& opt : m_options) {
            if (opt->has_short() && !valid_short(opt->shortname(), rule)) {
                msg << "short option -" << opt->shortname() << ".";
                throw std::invalid_argument(msg.str());
            }
            if (!valid_long(opt->longname(), rule)) {
                msg << "long option --" << opt->longname() << ".";
                throw std::invalid_argument(msg.str());
            }
        }
    }
    m_opt_rule = rule;
}

/**
 * @brief Sets whether or not the parser should trigger an exit on error.
 * @param eoe If set to true, then the parse will be configured to exit on
 *            error. If set to false, then the parser will be configured to
 *            instead raise an exception.
 *
 * @see error
 * @see parse
 */
void parser::exit_on_error(bool eoe)
{
    m_exit_on_error = eoe;
}


/**
 * @brief Parses an option.
 * @param argv Unmodified input argument vector.
 * @param offset Index in the input argument vector to look at.
 * @return The number of arguments from argv that was parsed. If no option was
 *         parsed, then 0 is returned.
 *
 * @note Internal cppopt usage only.
 *
 * @see parse
 */
std::size_t parser::parse_option(const std::vector<std::string>& argv, std::size_t offset)
{
    const std::string& arg = argv[offset];
    std::size_t parsed_args = 0;

    if (arg.length() >= 2 && arg.substr(0, 2) == "--") {
        // Long option
        std::string argname;
        std::string next_param = "";

        // Scan the name, find eventual equals sign
        auto eqpos = arg.find('=');
        if (eqpos != std::string::npos) {
            argname = arg.substr(2, eqpos - 2);
            next_param = arg.substr(eqpos + 1);
        } else {
            argname = arg.substr(2);
        }

        // Check that name exists
        auto opt_it = m_longmap.find(argname);
        if (opt_it == m_longmap.end()) {
            std::ostringstream msg;
            msg << "Unrecognized long option --" << argname << ".";
            this->error(msg.str());
        }

        std::string errname = "long option --" + argname + "";
        parsed_args = this->parse_option_parameters(argv,
                                                    offset + 1,
                                                    m_options[opt_it->second],
                                                    errname,
                                                    next_param);
        parsed_args += 1;
    } else if (arg.length() >= 1 && arg[0] == '-') {
        // Short option
        if (arg.length() == 1) {
            this->error("Missing short option specifiers. "
                        "Only a single - provided.");
        }

        for (std::size_t i = 1; i < arg.length(); ++i) {
            char opt_c = arg[i];

            // Check that name exists
            auto opt_it = m_shortmap.find(opt_c);
            if (opt_it == m_shortmap.end()) {
                std::ostringstream msg;
                msg << "Unrecognized short option -" << opt_c << ".";
                this->error(msg.str());
            }

            std::shared_ptr<option_base> opt = m_options[opt_it->second];
            if (opt->param_count() > 0) {
                // Extract potential compount argument
                std::string next_param = arg.substr(i + 1);
                if (next_param.length() > 0 && next_param[0] == '=')
                    next_param = arg.substr(i + 2);

                std::ostringstream errname;
                errname << "short option -" << opt_c << "";
                parsed_args = this->parse_option_parameters(argv,
                                                            offset + 1,
                                                            opt,
                                                            errname.str(),
                                                            next_param);
                break;
            } else {
                // No parameters accepted by this short option. Toggle it and
                // move on to the next short option in the same argument.
                try {
                    opt->toggle({});
                } catch (const invalid_usage& e) {
                    std::ostringstream msg;
                    msg << "Error toggling short option -" << opt_c << ": "
                        << e.what();
                    this->error(msg.str());
                } catch (const invalid_cast& e) {
                    std::ostringstream msg;
                    msg << "Error in parameter for short option -" << opt_c
                        << ": " << e.what();
                    this->error(msg.str());
                }
            }
        }
        parsed_args += 1;
    }

    return parsed_args;
}


/**
 * @brief Parses parameters to an option.
 * @param argv Unmodified input argument vector.
 * @param offset Index in the input argument vector to start looking for
 *               options. If a compound parameter is provided, then that
 *               parameter will be checked first before checking input
 *               arguments in argv.
 * @param opt Pointer to the option to parse parameters for.
 * @param errname Short descriptive name to use for opt in error messages.
 * @param compount_param An option parameter that was conjoined with the option
 *                       name is the same string. If no compound parameter was
 *                       specified, then this should be the empty string.
 * @return The number of arguments from argv that was parsed. If no parameter
 *         from argv was parsed, then 0 is returned. This count does not
 *         include the compound parameter.
 *
 * @note Internal cppopt usage only.
 *
 * @see parse_option
 */
std::size_t parser::parse_option_parameters(const std::vector<std::string>& argv,
                                            std::size_t offset,
                                            std::shared_ptr<option_base> opt,
                                            const std::string& errname,
                                            const std::string& compount_param)
{
    std::vector<std::string> optargs;
    std::size_t required_args = 0;

    if (opt->param_count() == 0) {
        // If no parameters were expected, then just sanity check that no
        // compound parameter was provided.
        if (compount_param.length() > 0) {
            std::ostringstream msg;
            msg << "Cannot pass parameter \"" << compount_param << "\" to "
                << errname << " that accepts no parameters.";
            this->error(msg.str());
        }
    } else {
        // Check the number of required args from argv
        required_args = opt->param_count();
        if (compount_param.length() > 0)
            required_args -= 1;

        std::size_t remaining_args = argv.size() - offset;
        if (remaining_args < required_args) {
            std::ostringstream msg;
            msg << "Too few arguments remaining for " << errname << ". Expected at"
                << " least " << required_args << " arguments, but only "
                << remaining_args << " arguments remains.";
            this->error(msg.str());
        }

        if (compount_param.length() > 0)
            optargs.push_back(compount_param);

        for (std::size_t i = 0; i < required_args; ++i)
            optargs.push_back(argv[offset + i]);
    }

    // Toggle the option, covers both cases above
    try {
        opt->toggle(optargs);
    } catch (const invalid_usage& e) {
        std::ostringstream msg;
        msg << "Error toggling " << errname << ": " << e.what();
        this->error(msg.str());
    } catch (const invalid_cast& e) {
        std::ostringstream msg;
        msg << "Error in parameter for " << errname << ": " << e.what();
        this->error(msg.str());
    }

    return required_args;
}


/**
 * @brief Adds an option to the parser.
 * @param opt Reference to the option to add. Will create a copy of this option
 *            that is then added to the parser.
 *
 * @throws std::invalid_argument If the entered option does not comply with the
 *                               option naming rules.
 *
 * @note Internal cppopt usage only.
 *
 * @see add
 */
void parser::add_individual(const option_base& opt)
{
    // Check for name validity
    if (opt.has_short() && !valid_short(opt.shortname(), m_opt_rule)) {
        std::ostringstream msg;
        msg << "Invalid short name -" << opt.shortname() << " for configured "
            << "option naming rule "
            << option_naming_rule_to_string(m_opt_rule) << ".";
        throw std::invalid_argument(msg.str());
    }
    if (!valid_long(opt.longname(), m_opt_rule)) {
        std::ostringstream msg;
        msg << "Invalid long name --" << opt.longname() << " for configured "
            << "option naming rule "
            << option_naming_rule_to_string(m_opt_rule) << ".";
        throw std::invalid_argument(msg.str());
    }

    // Perform all conflict checks before updating any parser variables
    if (opt.has_short()) {
        auto search = m_shortmap.find(opt.shortname());
        if (search != m_shortmap.end()) {
            std::ostringstream msg;
            msg << "Duplicate usage of option short name \'" << opt.shortname() << "\', "
                << "already used by the \"" << m_options[search->second]->longname() << "\" "
                << "option";
            throw std::invalid_argument(msg.str());
        }
    }
    if (m_longmap.find(opt.longname()) != m_longmap.end()) {
        std::ostringstream msg;
        msg << "Duplicate usage of option long name \"" << opt.longname() << "\"";
        throw std::invalid_argument(msg.str());
    }

    m_options.push_back(opt.copy());
    std::size_t idx = m_options.size() - 1;

    if (opt.has_short())
        m_shortmap[opt.shortname()] = idx;

    m_longmap[opt.longname()] = idx;
}


/**
 * @brief Adds a positional to the parser.
 * @param opt Reference to the positional to add. Will create a copy of this
 *            positional that is then added to the parser.
 *
 * @throws std::invalid_argument If the entered positional can result in an
 *                               ambiguous or unsatisfiable parsing state.
 *
 * @note Internal cppopt usage only.
 *
 * @see add
 */
void parser::add_individual(const positional_base& pos)
{
    if (m_positionals.size() > 0) {
        const std::shared_ptr<positional_base>& lastpos = m_positionals[m_positionals.size() - 1];
        if (lastpos->optional() && !pos.optional()) {
            std::ostringstream msg;
            msg << "Cannot have required positional(s)";
            for (const std::string& mv : pos.metavars())
                msg << ' ' << mv;
            msg << " follow optional positional(s)";
            for (const std::string& mv : lastpos->metavars())
                msg << ' ' << mv;
            throw std::invalid_argument(msg.str());
        }

        if (lastpos->multi_togglable()) {
            std::ostringstream msg;
            msg << "Cannot have positional(s)";
            for (const std::string& mv : pos.metavars())
                msg << ' ' << mv;
            msg << " follow multi-togglable positional(s)";
            for (const std::string& mv : lastpos->metavars())
                msg << ' ' << mv;
            throw std::invalid_argument(msg.str());
        }
    }

    m_positionals.push_back(pos.copy());
}


} /* namespace cppopt */
