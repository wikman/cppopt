/**
 * @file   naming.cpp
 * @author John Wikman
 * @brief  Implements option naming utilities.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string>

#include <cppopt/naming.hpp>

namespace cppopt {

/**
 * @brief Converts a cppopt::option_naming_rule to a string representation.
 * @param rule The option naming rule.
 * @return Corresponding string representation.
 */
const std::string& option_naming_rule_to_string(option_naming_rule rule) noexcept
{
    static const std::string r_invalid = "INVALID_NAMING_RULE";
    static const std::string r_default = "OPTNAME_DEFAULT";
    static const std::string r_alphanumeric = "OPTNAME_ALPHANUMERIC";

    switch (rule) {
    case OPTNAME_DEFAULT:
        return r_default;
    case OPTNAME_ALPHANUMERIC:
        return r_alphanumeric;
    }

    return r_invalid;
}

/**
 * @brief Checks if a short option name is valid.
 * @param c The short option name to check.
 * @param rule The option naming rule it should adhere to.
 * @return true if it is valid, else false.
 */
bool valid_short(char c, option_naming_rule rule) noexcept
{
    bool allow_uppercase = false;
    bool allow_lowercase = false;
    bool allow_digits = false;
    bool allow_basicsymbols = false;

    // Convert naming rule to the allowed character sets
    switch (rule) {
    case OPTNAME_DEFAULT:
        allow_uppercase = true;
        allow_lowercase = true;
        allow_digits = true;
        allow_basicsymbols = true;
        break;
    case OPTNAME_ALPHANUMERIC:
        allow_uppercase = true;
        allow_lowercase = true;
        allow_digits = true;
        break;
    }

    if (c >= 'a' && c <= 'z')
        return allow_lowercase;
    if (c >= 'A' && c <= 'Z')
        return allow_uppercase;
    if (c >= '0' && c <= '9')
        return allow_digits;

    switch (c) {
    case '?':
    case '!':
    case '%':
    case '+':
    case '_':
    case ':':
    case ';':
    case '|':
    case '>':
    case '<':
        return allow_basicsymbols;
    default:
        break;
    }

    return false;
}

/**
 * @brief Checks if the long option name is valid.
 * @param s The long option name to check.
 * @param rule The option naming rule it should adhere to.
 * @return true if it is valid, else false.
 */
bool valid_long(const std::string& s, option_naming_rule rule) noexcept
{
    bool allow_interleaving_hyphen = false;
    bool allow_empty_string = false;

    switch (rule) {
    case OPTNAME_DEFAULT:
        allow_interleaving_hyphen = true;
        break;
    case OPTNAME_ALPHANUMERIC:
        allow_interleaving_hyphen = true;
        break;
    }

    if (s.length() == 0)
        return allow_empty_string;

    // Check individual characters one by one. Return false at the first sight
    // of an invalid character.
    for (std::size_t i = 0; i < s.length(); ++i) {
        // Check interleaving hyphen
        if ((s[i] == '-') && (i != 0) && (i != s.length() - 1)) {
            if (allow_interleaving_hyphen)
                continue;
            else
                return false;
        }

        // Reuse function for short names
        if (!valid_short(s[i], rule))
            return false;
    }

    // All checks in the loop passed.
    return true;
}

} /* namespace cppopt */
