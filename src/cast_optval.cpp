/**
 * @file   cast_optval.cpp
 * @author John Wikman
 * @brief  Implements predefined parameter to type conversions.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cerrno>
#include <climits>
#include <cstdint>
#include <cstdlib>
#include <string>

#include <cppopt/cast_optval.hpp>
#include <cppopt/exception.hpp>

namespace cppopt {


/**
 * @brief Converts a parameter to a `char`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `char`.
 *
 * @throws cppopt::invalid_cast If param is not a single character string.
 */
template <>
char cast_optval<char>(const std::string& param)
{
    if (param.length() != 1)
        throw invalid_cast("parameter that is not a single character cannot "
                           "be converted to char");

    return param[0];
}


/**
 * @brief Convert to any integer type from a parameter.
 * @param param The parameter to convert.
 * @param name The name of the type that is being converted from a string. Used
 *             for printing more helpful error messages.
 * @return The input parameter converted to the specified `IntType`.
 *
 * @note Internal function only. This is not exposed through the public API.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), hexadecimal
 * format (e.g. "0x2c"), or binary format (e.g. "0b101100"). If the integer
 * type allows for negation, it can also be specified in negative decimal
 * format (e.g. -44).
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside the space allocated for the provided
 *                              `IntType`.
 */
template <typename IntType,
          IntType max_value,
          IntType min_value,
          bool allow_negation>
static IntType cast_optval_intval(const std::string& param, const std::string& name)
{
    // Define the danger zones. If we are within these values then we do not
    // have to check for whether we can fit another digit. I.e. anything
    // less than or greater than this can still be multiplied by 10 and have
    // another digit added or subtracted.
    // Example: std::int16_t
    //   pos_danger_zone = (32767 - (32767 % 10)) / 10 = 32760 / 10 = 3276
    //      v = 3275
    //      v * 10 + 9 = (3275 * 10) + 9 = 32750 + 9 = 32759 < 32767
    //   neg_danger_zone = (-32768 - (-32768 % 10)) / 10 = -32760 / 10 = -3276
    //      v = -3275
    //      v * 10 - 9 = (-3275 * 10) - 9 = -32750 - 9 = -32759 > -32768
    constexpr IntType pos_danger_zone = (max_value - (max_value % 10)) / 10;
    constexpr IntType neg_danger_zone = (min_value - (min_value % 10)) / 10;
    constexpr IntType pos_danger_digit = max_value % 10;
    constexpr IntType neg_danger_digit = -(min_value % 10);

    constexpr std::size_t max_hex_digits = sizeof(IntType) * 2;
    constexpr std::size_t max_binary_digits = sizeof(IntType) * 8;

    IntType ret = 0;

    if (param.length() == 0)
        throw invalid_cast("empty parameter cannot be converted to " + name);

    if (param.length() >= 2 && (param.substr(0,2) == "0x" || param.substr(0,2) == "0X")) {
        // Input format: Hexadecimal
        if (param.length() < 3)
            throw invalid_cast(name + " is missing hexadecimal digits");
        if (param.length() > (2 + max_hex_digits))
            throw invalid_cast("hexadecimal parameter cannot fit within a type " + name);

        for (std::size_t i = 2; i < param.length(); ++i) {
            IntType digitval = 0;
            if (param[i] >= '0'  && param[i] <= '9')
                digitval = param[i] - '0';
            else if (param[i] >= 'a' && param[i] <= 'f')
                digitval = 10 + (param[i] - 'a');
            else if (param[i] >= 'A' && param[i] <= 'F')
                digitval = 10 + (param[i] - 'A');
            else
                throw invalid_cast(name + " parameter contains non-hexadecimal digit characters");

            ret = (ret * 16) + digitval;
        }
    } else if (param.length() >= 2 && (param.substr(0,2) == "0b" || param.substr(0,2) == "0B")) {
        // Input format: Binary
        if (param.length() < 3)
            throw invalid_cast(name + " is missing binary digits");
        if (param.length() > (2 + max_binary_digits))
            throw invalid_cast("binary parameter cannot fit within a type " + name);

        for (std::size_t i = 2; i < param.length(); ++i) {
            IntType digitval = 0;
            if (param[i] >= '0'  && param[i] <= '1')
                digitval = param[i] - '0';
            else
                throw invalid_cast(name + " parameter contains non-hexadecimal digit characters");

            ret = (ret * 2) + digitval;
        }
    } else if (param.length() >= 1 && param.substr(0,1) == "-") {
        // Input format: Negative decimal
        if (!allow_negation)
            throw invalid_cast(name + " cannot be specified by a negative number");
        if (param.length() < 2)
            throw invalid_cast(name + " parameter is missing digits");
        for (std::size_t i = 1; i < param.length(); ++i) {
            if (param[i] < '0' || param[i] > '9')
                throw invalid_cast(name + " parameter contains non-decimal digit characters");
        }

        ret = -(param[1] - '0');
        for (std::size_t i = 2; i < param.length(); ++i) {
            if (ret > neg_danger_zone) {
                // We know for certain that we can fit another digit
                ret = (ret * 10) - (param[i] - '0');
                continue;
            }

            if (ret == neg_danger_zone) {
                if ((IntType) (param[i] - '0') > neg_danger_digit)
                    throw invalid_cast("parameter cannot fit within a type " + name);

                // It can just about fit another digit
                ret = (ret * 10) - (param[i] - '0');
                continue;
            } else {
                throw invalid_cast("parameter cannot fit within a type " + name);
            }
        }
    } else {
        // Input format: Positive decimal
        if (param.length() < 1)
            throw invalid_cast(name + " parameter is missing digits");
        for (std::size_t i = 0; i < param.length(); ++i) {
            if (param[i] < '0' || param[i] > '9')
                throw invalid_cast(name + " parameter contains non-decimal digit characters");
        }

        ret = param[0] - '0';
        for (std::size_t i = 1; i < param.length(); ++i) {
            if (ret < pos_danger_zone) {
                // We know for certain that we can fit another digit
                ret = (ret * 10) + (param[i] - '0');
                continue;
            }

            if (ret == pos_danger_zone) {
                if ((IntType) (param[i] - '0') > pos_danger_digit)
                    throw invalid_cast("parameter cannot fit within a type " + name);

                // It can just about fit another digit
                ret = (ret * 10) + (param[i] - '0');
                continue;
            } else {
                throw invalid_cast("parameter cannot fit within a type " + name);
            }
        }
    }

    return ret;
}


/**
 * @brief Converts a parameter to `int8_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `int8_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), negative
 * decimal format (e.g. -44), hexadecimal format (e.g. "0x2c"), or binary
 * format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `int8_t`.
 */
template <> std::int8_t cast_optval<std::int8_t>(const std::string& param)
{
    return cast_optval_intval<std::int8_t, INT8_MAX, INT8_MIN, true>(param, "int8_t");
}


/**
 * @brief Converts a parameter to `uint8_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to an `uint8_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), hexadecimal
 * format (e.g. "0x2c"), or binary format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `uint8_t`.
 */
template <> std::uint8_t cast_optval<std::uint8_t>(const std::string& param)
{
    return cast_optval_intval<std::uint8_t, UINT8_MAX, 0, false>(param, "uint8_t");
}


/**
 * @brief Converts a parameter to `int16_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `int16_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), negative
 * decimal format (e.g. -44), hexadecimal format (e.g. "0x2c"), or binary
 * format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `int16_t`.
 */
template <> std::int16_t cast_optval<std::int16_t>(const std::string& param)
{
    return cast_optval_intval<std::int16_t, INT16_MAX, INT16_MIN, true>(param, "int16_t");
}


/**
 * @brief Converts a parameter to `uint16_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to an `uint16_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), hexadecimal
 * format (e.g. "0x2c"), or binary format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `uint16_t`.
 */
template <> std::uint16_t cast_optval<std::uint16_t>(const std::string& param)
{
    return cast_optval_intval<std::uint16_t, UINT16_MAX, 0, false>(param, "uint16_t");
}


/**
 * @brief Converts a parameter to `int32_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `int32_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), negative
 * decimal format (e.g. -44), hexadecimal format (e.g. "0x2c"), or binary
 * format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `int32_t`.
 */
template <> std::int32_t cast_optval<std::int32_t>(const std::string& param)
{
    return cast_optval_intval<std::int32_t, INT32_MAX, INT32_MIN, true>(param, "int32_t");
}


/**
 * @brief Converts a parameter to `uint32_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to an `uint32_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), hexadecimal
 * format (e.g. "0x2c"), or binary format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `uint32_t`.
 */
template <> std::uint32_t cast_optval<std::uint32_t>(const std::string& param)
{
    return cast_optval_intval<std::uint32_t, UINT32_MAX, 0, false>(param, "uint32_t");
}


/**
 * @brief Converts a parameter to `int64_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `int64_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), negative
 * decimal format (e.g. -44), hexadecimal format (e.g. "0x2c"), or binary
 * format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `int64_t`.
 */
template <> std::int64_t cast_optval<std::int64_t>(const std::string& param)
{
    return cast_optval_intval<std::int64_t, INT64_MAX, INT64_MIN, true>(param, "int64_t");
}


/**
 * @brief Converts a parameter to `uint64_t`.
 * @param param The parameter to convert.
 * @return The input parameter converted to an `uint64_t`.
 *
 * Allows integers to be specified in decimal format (e.g. "44"), hexadecimal
 * format (e.g. "0x2c"), or binary format (e.g. "0b101100").
 *
 * @throws cppopt::invalid_cast If param does not does not follow the specified
 *                              format or if the converted value cannot fit
 *                              inside an `uint64_t`.
 */
template <> std::uint64_t cast_optval<std::uint64_t>(const std::string& param)
{
    return cast_optval_intval<std::uint64_t, UINT64_MAX, 0, false>(param, "uint64_t");
}


/**
 * @todo Generic float conversion from a parameter.
 * @param param The parameter to convert.
 * @param name The name of the type that is being converted from a string. Used
 *             for printing more helpful error messages.
 * @return The input parameter converted to the specified `FloatType`.
 *
 * See std::strtof in the standard library for formatting rules.
 *
 * @note Acting as a wrapper function for the underlying ConvFunc that is
 *       assumed to behave in accordance with std::strtof.
 * @note Internal function only. This is not exposed through the public API.
 *
 * @throws cppopt::invalid_cast If param does not does not follow a standard
 *                              float format, if the converted value cannot fit
 *                              inside the provided `FloatType`, or if the
 *                              parameter contains characters not used in the
 *                              conversion.
 */
template <typename FloatType,
          FloatType (*ConvFunc)(const char*, char**)>
static FloatType cast_optval_floatval(const std::string& param, const std::string& name)
{
    const char *s = param.c_str();
    char* end = nullptr;
    if (param.length() == 0)
        throw invalid_cast(name + " value cannot be represented by empty string");
    if (std::isspace((unsigned char) s[0]))
        throw invalid_cast(name + " value contains unused characters");

    FloatType ret = ConvFunc(s, &end);
    if (errno == ERANGE) {
        errno = 0;
        throw invalid_cast(name + " value out of range");
    }

    if ((s + param.length()) != end)
        throw invalid_cast(name + " value contains unused or invalid characters");

    return ret;
}


/**
 * @brief Converts a parameter to `float`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `float`.
 *
 * Acts as a wrapper around std::strtof from the standard library. See
 * documentation for that function for formatting rules.
 *
 * @throws cppopt::invalid_cast If param does not does not follow the format
 *                              for std::strtof, if the converted value cannot
 *                              fit inside a `float`, or if the parameter
 *                              contains characters not used in the conversion.
 */
template <> float cast_optval<float>(const std::string& param)
{
    return cast_optval_floatval<float, std::strtof>(param, "float");
}


/**
 * @brief Converts a parameter to `double`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `double`.
 *
 * Acts as a wrapper around std::strtod from the standard library. See
 * documentation for that function for formatting rules.
 *
 * @throws cppopt::invalid_cast If param does not does not follow the format
 *                              for std::strtod, if the converted value cannot
 *                              fit inside a `double`, or if the parameter
 *                              contains characters not used in the conversion.
 */
template <> double cast_optval<double>(const std::string& param)
{
    return cast_optval_floatval<double, std::strtod>(param, "double");
}


/**
 * @brief Converts a parameter to `long double`.
 * @param param The parameter to convert.
 * @return The input parameter converted to a `long double`.
 *
 * Acts as a wrapper around std::strtold from the standard library. See
 * documentation for that function for formatting rules.
 *
 * @throws cppopt::invalid_cast If param does not does not follow the format
 *                              for std::strtold, if the converted value cannot
 *                              fit inside a `long double`, or if the parameter
 *                              contains characters not used in the conversion.
 */
template <> long double cast_optval<long double>(const std::string& param)
{
    return cast_optval_floatval<long double, std::strtold>(param, "long double");
}


} /* namespace cppopt */
