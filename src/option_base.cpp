/**
 * @file   option_base.cpp
 * @author John Wikman
 * @brief  Implements functions for base class inherited by all options.
 *
 * MIT License
 *
 * Copyright (c) 2021 John Wikman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <memory>
#include <stdexcept>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <cppopt/exception.hpp>
#include <cppopt/option_base.hpp>

namespace cppopt {

/**
 * @brief Full base constructor for an option base class including a short
 *        option name.
 * @param shortopt The short "single-character" option name (i.e. `-S`).
 *                 Specifying the null character as the short option name will
 *                 result in no short option name being available for this
 *                 option.
 * @param longopt Long option name. (i.e. `--longopt`)
 * @param description Description shown when printing the help message.
 * @param metavars Meta variable association to parameters for this option. The
 *                 length of this array determines how many parameters that
 *                 this option accepts.
 * @param mod Modifiers to apply to this option. Currently supported modifiers
 *            are:
 *             - REQUIRED \n
 *               Makes the cppopt::option_base::required() function return
 *               true. Specify this modifier if this option must be provided to
 *               the parser.
 *
 * @note This should only ever be invoked by subclasses of option.
 */
option_base::option_base(char shortopt, const std::string& longopt,
                         const std::vector<std::string>& metavars,
                         const std::string& description,
                         modifier mod):
    m_short(shortopt),
    m_long(longopt),
    m_description(description),
    m_modifiers(mod),
    m_metavars(metavars),
    m_enum_values(metavars.size(), std::set<std::string>()),
    m_times_toggled(0)
{
    // Do nothing here, name validity is performed by the parser
}

/**
 * @brief Base constructor for an option base class that only has a long name
 *        associated with it.
 * @param longopt Long option name. (i.e. `--longopt`)
 * @param description Description shown when printing the help message.
 * @param metavars Meta variable association to parameters for this option. The
 *                 length of this array determines how many parameters that
 *                 this option accepts.
 * @param mod Modifiers to apply to this option. Currently supported modifiers
 *            are:
 *             - REQUIRED \n
 *               Makes the cppopt::option_base::required() function return
 *               true. Specify this modifier if this option must be provided to
 *               the parser.
 *
 * @note This should only ever be invoked by subclasses of option.
 */
option_base::option_base(const std::string& longopt,
                         const std::vector<std::string>& metavars,
                         const std::string& description,
                         modifier mod):
    option_base('\0', longopt,
                metavars,
                description,
                mod)
{
    // Do nothing here, name validity is performed by the parser
}

/**
 * @brief Destructor.
 */
option_base::~option_base() {}

/**
 * @brief Copy-constructor.
 * @param other The option_base object to copy.
 */
option_base::option_base(const option_base& other):
    m_short(other.m_short),
    m_long(other.m_long),
    m_description(other.m_description),
    m_modifiers(other.m_modifiers),
    m_metavars(other.m_metavars),
    m_enum_values(other.m_enum_values),
    m_times_toggled(other.m_times_toggled)
{
    // No special behavior
}

/**
 * @brief Move-constructor.
 * @param other The option_base object to move into this object.
 */
option_base::option_base(option_base&& other) noexcept
{
    std::swap(m_short, other.m_short);
    std::swap(m_long, other.m_long);
    std::swap(m_description, other.m_description);
    std::swap(m_modifiers, other.m_modifiers);
    std::swap(m_metavars, other.m_metavars);
    std::swap(m_enum_values, other.m_enum_values);
    std::swap(m_times_toggled, other.m_times_toggled);
}

/**
 * @brief Copy-assignment.
 * @param other The option_base object to copy.
 * @return A reference to this object after the copy operation.
 */
option_base& option_base::operator=(const option_base& other)
{
    *this = option_base(other);
    return *this;
}

/**
 * @brief Move-assignment.
 * @param other The option_base object to move into this object.
 * @return A reference to this object after the move operation.
 */
option_base& option_base::operator=(option_base&& other) noexcept
{
    *this = option_base(other);
    return *this;
}

/**
 * @brief Returns a dynamically allocated copy of the option_base object.
 * @return An std::shared_ptr containing a dynamically allocated pointer to a
 *         cppopt::option_base object.
 * @note This is virtual as to indicate that subclasses will copied
 *       differently.
 */
std::shared_ptr<option_base> option_base::copy() const
{
    return std::shared_ptr<option_base>(new option_base(*this));
}

/**
 * @brief Toggle this option.
 * @param params The parameters to toggle this option on.
 *
 * @throws cppopt::invalid_usage If the number of parameters provided do not
 *                               match the number of configured parameters for
 *                               this option.
 * @throws cppopt::invalid_cast If one of the parameters does not match one of
 *                              its enumerated values (if configured), or if
 *                              something went wrong converting a parameter to
 *                              a type.
 */
void option_base::toggle(const std::vector<std::string>& params)
{
    if (this->param_count() != params.size()) {
        std::ostringstream msg;
        msg << "Number of provided parameters does not match the number of "
            << "accepted parameters. " << params.size() << " provided. "
            << "Exactly " << this->param_count() << " parameter(s) accepted.";
        throw cppopt::invalid_usage(msg.str());
    }

    // Check for enumerated values
    for (std::size_t i = 0; i < params.size(); ++i) {
        if (m_enum_values[i].size() == 0) {
            // No enums for this parameter
            continue;
        }

        if (m_enum_values[i].find(params[i]) == m_enum_values[i].end())
            throw cppopt::invalid_cast("\"" + params[i] + "\" does not match any specified enum value");
    }

    m_internal_toggle(params);
    m_times_toggled += 1;
}

/**
 * @brief The internal toggle function. This should never be invoked directly
 *        on the cppopt::option_base class.
 * @param params Parameters for this option.
 *
 * @throws cppopt::invalid_usage If invoked. This should only ever be invoked
 *                               on subclasses of option_base.
 */
void option_base::m_internal_toggle(const std::vector<std::string>& params)
{
    (void) params;

    throw cppopt::invalid_usage("toggle() invoked directly on the cppopt::option_base class");
}

/**
 * @brief Sets enumerated values for each of the parameters.
 * @param enum_values Vector containing sets of enumerated values for each
 *                    option parameter.
 *
 * @note The empty set indicates that any input value is valid.
 *
 * @warning Will overwrite any previously configured enumerated values.
 *
 * @throws std::invalid_argument If length of enum_values does not match the
 *                               number of configured parameters for this
 *                               option.
 */
void option_base::set_enums(const std::vector<std::set<std::string>>& enum_values)
{
    if (enum_values.size() != this->param_count())
        throw std::invalid_argument("length of enumeration vector does not match the number of option parameters");

    m_enum_values = enum_values;
}

/**
 * @brief Return true if this option has a short name, otherwise false.
 */
bool option_base::has_short() const
{
    return m_short != '\0';
}

/**
 * @brief Returns the short name for this option.
 */
char option_base::shortname() const
{
    return m_short;
}

/**
 * @brief Returns the long name for this option.
 */
const std::string& option_base::longname() const
{
    return m_long;
}

/**
 * @brief Returns the description for this option.
 */
const std::string& option_base::description() const
{
    return m_description;
}

/**
 * @brief Returns the metavars for this option.
 */
const std::vector<std::string>& option_base::metavars() const
{
    return m_metavars;
}

/**
 * @brief Returns the enumerated values for this option.
 */
const std::vector<std::set<std::string>>& option_base::enums() const
{
    return m_enum_values;
}

/**
 * @brief Return the number of parameters accepted by this option.
 */
std::size_t option_base::param_count() const
{
    return m_metavars.size();
}

/**
 * @brief Returns the modifiers of this option.
 */
modifier option_base::modifiers() const
{
    return m_modifiers;
}

/**
 * @brief Returns true if this option has been toggled, otherwise false.
 */
bool option_base::toggled() const
{
    return m_times_toggled > 0;
}

/**
 * @brief Returns the number of times that this option has been toggled.
 */
std::size_t option_base::toggle_count() const
{
    return m_times_toggled;
}

/**
 * @brief Returns true if this is a required option. Else false.
 */
bool option_base::required() const
{
    return (m_modifiers & REQUIRED) == REQUIRED;
}

} /* namespace cppopt */
