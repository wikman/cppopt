.PHONY: build-lib coherence-check html \
        unittests unittests-checkmemleak \
        clean

SHELL=sh

BUILDDIR=build
DISTDIR=dist

LIB-SO=libcppopt.so

SRCFILES=src/*.cpp

VERSION=0.1.1

CXX=g++
CXXFLAGS=--std=c++11 -Iinclude -Wall -Wextra

$(DISTDIR)/$(LIB-SO):
	mkdir -p $(BUILDDIR)
	mkdir -p $(DISTDIR)
	$(CXX) $(CXXFLAGS) -O3 -fPIC --shared $(SRCFILES) -o $(BUILDDIR)/libcppopt.so
	mv $(BUILDDIR)/libcppopt.so $(DISTDIR)/$(LIB-SO)
build-lib: $(DISTDIR)/$(LIB-SO)

# Checks for coherence between values defined in the library vs. values
# defines here.
coherence-check: $(DISTDIR)/$(LIB-SO)
	$(eval VERFILE := $(BUILDDIR)/vercheck.cpp)
	$(eval VEROUT := $(BUILDDIR)/vercheck.out)
	$(eval LICFILE := $(BUILDDIR)/license-check.cpp)
	$(eval LICOUT := $(BUILDDIR)/license-check.out)
	mkdir -p $(BUILDDIR)

	# Check version number
	@echo -e "#include <iostream>" > $(VERFILE)
	@echo -e "#include <string>"   >> $(VERFILE)
	@echo -e "#include <cppopt.h>" >> $(VERFILE)
	@echo -e "int main(){std::cout<<cppopt::version()<<std::endl;return 0;}" >> $(VERFILE)
	$(CXX) $(CXXFLAGS) -L$(DISTDIR) $(VERFILE) -lcppopt -o $(VEROUT)
	@echo "$(VERSION)" > $(BUILDDIR)/VERSION
	LD_LIBRARY_PATH=$(DISTDIR) ./$(VEROUT) | diff $(BUILDDIR)/VERSION -

	# Check license notice
	find src include -type f -exec python3 test/scripts/check_license.py LICENSE {} \+

format-check:
	$(eval FMTOPTS := --no-trailing-whitespace --no-tab --no-crlf)
	# Check source code formatting
	find src include -type f -exec python3 test/scripts/check_format.py $(FMTOPTS) {} \+

unittests: $(DISTDIR)/$(LIB-SO)
	$(eval TESTFILES := test/unittests/test_*.cpp)
	mkdir -p $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -L$(DISTDIR) $(TESTFILES) -lcppopt -lgtest -lgtest_main -o $(BUILDDIR)/unittests
	LD_LIBRARY_PATH=$(DISTDIR) ./$(BUILDDIR)/unittests

unittests-checkmemleak: $(DISTDIR)/$(LIB-SO)
	$(eval TESTFILES := test/unittests/test_*.cpp)
	mkdir -p $(BUILDDIR)
	$(CXX) $(CXXFLAGS) -L$(DISTDIR) $(TESTFILES) -DUNITTESTS_CHECKMEMLEAK -lcppopt -lgtest -lgtest_main -o $(BUILDDIR)/unittests-checkmemleak
	LD_LIBRARY_PATH=$(DISTDIR) valgrind --tool=memcheck --leak-check=full --error-exitcode=1 ./$(BUILDDIR)/unittests-checkmemleak

clean:
	rm -rf $(BUILDDIR) $(DISTDIR)
