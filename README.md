# CppOpt

Argument parser library for C++. Heavily inspired by argparse for Python, but
adapted for C++ with the use of templates.

## Dependencies

### Usage

 - C++11 compliant C++ compiler.

### Testing & Development

 - doxygen
 - g++
 - git
 - gtest
 - make
 - python3
 - sass
 - valgrind

## TL;DR: Get started in a minute
An invocation to a file transfer program `myfile`:
```sh
myfile foo.txt --host=10.0.0.10 -p 22 -t1000 --dest-dir /var/opt/myfile bar.txt --no-confirm
```

Argument parser implementation:
```cpp
#include <cppopt.h>

int main(int argc, char *argv[])
{
    std::vector<std::string> source_files;

    std::string host;
    std::string destdir = "~/";
    std::uint16_t port = 22;
    std::size_t timeout_ms = 5000;
    bool no_confirm = false;

    // Description of the program is provided to the parser. Help message is by
    // default triggered by "-h" and "--help".
    auto parser = cppopt::parser("Transfer files to a host server.");
    parser.add(
        // The {"NAME"}-lists are meta-variables that are shown when printing the help message.
        cppopt::param<std::string>('H', "host", {"ADDR"}, "Host server", &host, cppopt::REQUIRED),
        cppopt::param<std::string>("dest-dir", {"PATH"}, "Destination directory", &destdir),
        cppopt::param<std::uint16_t>('p', "port", {"NUM"}, "Host port", &port),
        cppopt::param<std::size_t>('t', "timeout", {"MS"}, "Connection timeout", &timeout_ms),
        cppopt::flag("no-confirm", "Ignore confirmation.", &no_confirm),
        // Positional values
        cppopt::multi_positional<std::string>({"FILES"}, "Source files to send.", &source_files)
    );
    parser.parse(argc, argv);

    // Results:
    // source_files == {"foo.txt", "bar.txt"}
    // host == "10.0.0.10"
    // destdir == "/var/opt/myfile"
    // port == 22
    // timeout_ms == 1000
    // no_confirm == true
}
```

## Usage

```cpp
#include <cppopt/param.hpp>
#include <cppopt/parser.hpp>

// Setting of default values. These will be used by the parser if printing a
// help message.
std::string infile;
std::string std = "c99";
std::string logpath = "./myprog.log"
int cores = 1;
int line_min = -1;
int line_max = -1;

auto parser = cppopt::parser();

// Add a positional argument (order in which added matters for positionals)
parser.add(cppopt::positional<std::string>(
    "INFILE",
    "The input file to analyze.",
    &infile
));

// Add an option argument (order in which added does NOT matter for options)
auto opt_std = cppopt::param<std::string>(
    "std", {"STANDARD"},
    "Set the standard to use.",
    &std
);
// Will print an error if entered value is not one of these.
opt_std.set_enum({{"c99", "c11", "c++11", "c++20"}});
parser.add(opt_std);

// Add another option argument
parser.add(cppopt::param<int>(
    'c', "cores", {"N_CORES"},
    "Maximum number of cores to use in the analysis.",
    &cores
));

// Add another option argument
parser.add(cppopt::param<std::string>(
    "log-path", {"PATH"},
    "Where to store log output.",
    &logpath
));

// Add another option argument
parser.add(cppopt::param<int,int>(
    'r', "line-range", {"MIN", "MAX"},
    "The range of line numbers to scan.",
    &line_min, &line_max
));

// This will tell the parser to automatically print an error message and exit
// the program if something is wrong with the CLI arguments. Otherwise the
// returned variable from parser.parse() has to be checked manually and the
// error messages fetched from the parser class.
parser.enable_exit_on_error();
parser.parse({
    "./myprog", "-c", "4",
                "--std=c99",
                "--log-path=/var/log/myfile.log",
                "-r", "10", "42",
                "myfile.c"});
```
