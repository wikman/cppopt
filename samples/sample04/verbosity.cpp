/**
 * @file   verbosity.cpp
 * @author John Wikman
 * @brief  Sample: Options for specifying verbosity in a program.
 */

#include <iostream>
#include <string>
#include <vector>

#include <cppopt.h>

enum class verbosity {ERROR, WARNING, INFO, DEBUG};

std::ostream& operator<<(std::ostream& os, const verbosity& v)
{
    switch(v) {
    case verbosity::ERROR:   os << "ERROR";   break;
    case verbosity::WARNING: os << "WARNING"; break;
    case verbosity::INFO:    os << "INFO";    break;
    case verbosity::DEBUG:   os << "DEBUG";   break;
    default:
        os.setstate(std::ios_base::failbit);
        break;
    }
    return os;
}

verbosity& operator++(verbosity& v)
{
    switch (v) {
    case verbosity::ERROR:   v = verbosity::WARNING; break;
    case verbosity::WARNING: v = verbosity::INFO;    break;
    case verbosity::INFO:    v = verbosity::DEBUG;   break;
    default:
        break;
    }
    return v;
}


int main(int argc, char *argv[])
{
    std::vector<std::string> files;

    std::size_t counter = 0;
    verbosity v = verbosity::ERROR;

    auto parser = cppopt::parser("Sample program demonstrating the incrementer option.");
    parser.add(
        cppopt::incrementer<std::size_t>('c', "count", "Regular counter.", &counter),
        cppopt::incrementer<verbosity>('v', "verbose", "Custom typed verbosity specifier.", &v)
    );
    parser.add(
        cppopt::multi_positional<std::string>({"FILES"}, "Input files.", &files, cppopt::NONE)
    );
    parser.parse(argc, argv);

    std::cout << "Counter:   " << counter << std::endl;
    std::cout << "Verbosity: " << v << std::endl;
    std::cout << "Files:" << std::endl;
    for (const std::string &s : files)
        std::cout << " - " << s << std::endl;
}
