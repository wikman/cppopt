/**
 * @file   arith.cpp
 * @author John Wikman
 * @brief  Sample: Taking float arguments for an artihmetic calculation
 */

#include <iostream>
#include <string>
#include <vector>

#include <cppopt.h>

int main(int argc, char *argv[])
{
    std::string mode = "none";
    std::vector<float> numbers;
    float epsilon = 0.0;

    auto modearg = cppopt::positional<std::string>("MODE", "The program mode to use.", &mode);
    modearg.set_enums({{"sqrt", "floor", "ceil"}});

    auto parser = cppopt::parser("Perform the specific arithmetic computation.");
    parser.add(
        modearg,
        cppopt::multi_positional<float>(
            {"NUMBERS"}, "The numbers to compute on.", &numbers, cppopt::OPTIONAL
        ),
        cppopt::param<float>(
            'e', "epsilon", {"VALUE"},
            "Cutoff point to stop iterations on.", &epsilon
        )
    );
    parser.parse(argc, argv);

    std::cout << "Mode:    " << mode << std::endl;
    std::cout << "Epsilon: " << epsilon << std::endl;
    std::cout << "Numbers:" << std::endl;
    for (const float& f : numbers)
        std::cout << " - " << f << std::endl;
}
