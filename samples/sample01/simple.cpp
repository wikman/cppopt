/**
 * @file   simple.cpp
 * @author John Wikman
 * @brief  Sample: Options for a file transfer program.
 */

#include <functional>
#include <iostream>
#include <string>
#include <vector>

#include <cppopt.h>

int main(int argc, char *argv[])
{
    std::vector<std::string> source_files;

    std::string host;
    std::string destdir = "~/";
    std::uint16_t port = 22;
    std::size_t timeout_ms = 5000;
    bool no_confirm = false;

    // Description of the program is provided to the parser. Help message is by
    // default triggered by "-h" and "--help".
    auto parser = cppopt::parser("Transfer files to a host server.");
    parser.add(
        // The {"NAME"}-lists are meta-variables that are shown when printing the help message.
        cppopt::param<std::string>('H', "host", {"ADDR"}, "Host server", &host, cppopt::REQUIRED),
        cppopt::param<std::string>("dest-dir", {"PATH"}, "Destination directory", &destdir),
        cppopt::param<std::uint16_t>('p', "port", {"NUM"}, "Host port", &port),
        cppopt::param<std::size_t>('t', "timeout", {"MS"}, "Connection timeout", &timeout_ms),
        cppopt::flag("no-confirm", "Ignore confirmation.", &no_confirm)
    );
    parser.add(
        cppopt::multi_positional<std::string>({"FILES"}, "Source files to send.", &source_files)
    );
    parser.parse(argc, argv);

    std::cout << "Host:        " << host << std::endl;
    std::cout << "Destdir:     " << destdir << std::endl;
    std::cout << "Port:        " << port << std::endl;
    std::cout << "Timeout ms:  " << timeout_ms << std::endl;
    std::cout << "No confirm:  " << no_confirm << std::endl;
    std::cout << "Source files:" << std::endl;
    for (const std::string &s : source_files)
        std::cout << " - " << s << std::endl;
}
