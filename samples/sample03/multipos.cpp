/**
 * @file   multipos.cpp
 * @author John Wikman
 * @brief  Sample: Multi positional usage
 */

#include <iostream>
#include <sstream>
#include <string>

#include <cppopt.h>

// Sample custom type for parsing incoming IPv4 addresses
struct ipv4_address {
    std::uint8_t parts[4];

    std::string to_string() {
        std::ostringstream oss;
        oss << (int) parts[0] << '.'
            << (int) parts[1] << '.'
            << (int) parts[2] << '.'
            << (int) parts[3];
        return oss.str();
    }
};

// Explicit argument casting function. This could also be implicitly stated by
// implementing a constructor for ipv4_address that accepts a string argument.
namespace cppopt {
    template <>
    ipv4_address cast_optval<ipv4_address>(const std::string &param)
    {
        ipv4_address addr;

        auto d1idx = param.find('.', 0);
        if (d1idx == std::string::npos)
            throw cppopt::invalid_cast("invalid ipv4 address");
        auto d2idx = param.find('.', d1idx + 1);
        if (d2idx == std::string::npos)
            throw cppopt::invalid_cast("invalid ipv4 address");
        auto d3idx = param.find('.', d2idx + 1);
        if (d3idx == std::string::npos)
            throw cppopt::invalid_cast("invalid ipv4 address");

        try {
            addr.parts[0] = cast_optval<std::uint8_t>(param.substr(0, d1idx));
            addr.parts[1] = cast_optval<std::uint8_t>(param.substr(d1idx + 1, d2idx - (d1idx + 1)));
            addr.parts[2] = cast_optval<std::uint8_t>(param.substr(d2idx + 1, d3idx - (d2idx + 1)));
            addr.parts[3] = cast_optval<std::uint8_t>(param.substr(d3idx + 1));
        } catch (const invalid_cast& e) {
            throw invalid_cast("invalid ipv4 address");
        }

        return addr;
    }
}


int main(int argc, const char *argv[])
{
    ipv4_address host;
    std::vector<std::tuple<std::string, std::string>> src_dst_mapping;

    std::vector<std::tuple<std::string, std::string>> variable_mapping;

    std::size_t timeout_ms = 0;

    auto parser = cppopt::parser(
        "Connects to a host and transfers files from that machine to the "
        "localhost. Can specify multiple files to be transferred."
        "\n\n"
        "This is a sample forced linebreak. Anytime that \\n is specified in "
        "the description, it will always perform a linebreak."
    );
    parser.add(
        cppopt::param<std::size_t>(
            't', "timeout", {"MS"},
            "Connection timeout. The value 0 indicates that it should never"
            " timeout.",
            &timeout_ms
        ),
        cppopt::multi_param<std::string, std::string>(
            "set", {"NAME", "VALUE"},
            "Sets specific variable values, overriding the default variables"
            " defined in the configuration file.", 
            &variable_mapping
        )
    );
    parser.add(
        cppopt::positional<ipv4_address>(
            {"HOST"},
            "Host to connect to. Must be specified as an IPv4 address.",
            &host
        ),
        cppopt::multi_positional<std::string, std::string>(
            {"SOURCE_FILE", "TARGET_FILE"},
            "File transfers to perfrom from respective source files to target"
            " files.",
            &src_dst_mapping
        )
    );
    // Usually the optterm token is "--" is most software, but it can really be
    // anything we want.
    parser.set_optterm_token("==");
    parser.parse(argc, argv);

    // Manual sanity check.
    for (const auto& srcdst : src_dst_mapping) {
        if (std::get<0>(srcdst).length() == 0)
            parser.error("Source name cannot be an empty string");
        if (std::get<1>(srcdst).length() == 0)
            parser.error("Target name cannot be an empty string");
    }

    std::cout << "Timeout ms: " << timeout_ms << std::endl;
    std::cout << "Host:       " << host.to_string() << std::endl;
    std::cout << "Variable mappings:" << std::endl;
    if (variable_mapping.size() == 0) {
        std::cout << "  (No mappings specified)" << std::endl;
    } else {
        for (const auto& vars : variable_mapping)
            std::cout << "  - " << std::get<0>(vars) << " = " << std::get<1>(vars) << std::endl;
    }
    std::cout << "Source-Target mappings:" << std::endl;
    for (const auto& srcdst : src_dst_mapping)
        std::cout << "  - " << std::get<0>(srcdst) << " --> " << std::get<1>(srcdst) << std::endl;
}
