/**
 * @file   optterm.cpp
 * @author John Wikman
 * @brief  Sample: Terminating option parsing on token.
 */

#include <iostream>
#include <string>

#include <cppopt.h>

int main(int argc, char *argv[])
{
    std::string source;
    std::string target;

    std::size_t timeout_ms = 5000;
    bool ask_for_confirmation = true;

    auto parser = cppopt::parser("Transfer file from source to target.");
    parser.add(
        cppopt::param<std::size_t>('t', "timeout", {"MS"}, "Connection timeout", &timeout_ms),
        cppopt::flag('y', "yes", 
                     "Ignore confirmation. This is also a very long description"
                     " that should see some kind of line breaking when being"
                     " printed as a help message.",
                     &ask_for_confirmation)
    );
    parser.add(
        cppopt::positional<std::string>({"SOURCE"}, "Source file to transfer.", &source),
        cppopt::positional<std::string>({"TARGET"}, "Target destination.", &target)
    );
    // This allows source and target names to start with a "-" or "--".
    // Otherwise those values would be interpreted as options.
    parser.set_optterm_token("--");
    parser.parse(argc, argv);

    std::cout << "Timeout ms:  " << timeout_ms << std::endl;
    std::cout << "Req.confirm: " << ask_for_confirmation << std::endl;
    std::cout << "Source:      " << source << std::endl;
    std::cout << "Target:      " << target << std::endl;
}
